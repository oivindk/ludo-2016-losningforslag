package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;

/**
 * A PieceEvent is generated whenever a piece is moved on the board.
 * 
 * @author Øivind Kolloen
 *
 */
@SuppressWarnings("serial")
public class PieceEvent extends EventObject {
	private int player;
	private int piece;
	private int from;
	private int to;

	/**
	 * Creates a new piece event for the given game. Also include information about the player
	 * that moved the piece, what piece was moved, where the piece was moved from and where it
	 * was moved to.
	 * 
	 * @param ludo the game that generated this event.
	 * @param player the player that moved the piece.
	 * @param piece the piece that was moved.
	 * @param from where the piece was moved from.
	 * @param to where the piece was moved to.
	 */
	public PieceEvent(Ludo ludo, int player, int piece, int from, int to) {
		super(ludo);
		this.player = player;
		this.piece = piece;
		this.from = from;
		this.to = to;
	}
	
	/**
	 * Checks if two DiceEvent object refers to the same diceEvent.
	 * 
	 * @see java.lang.Object#equals(Object)
	 */
	@Override
	public boolean equals (Object o) {
		if (!(o instanceof PieceEvent)) {
			return false;
		}
		PieceEvent e = (PieceEvent)o;
		return (hashCode()==e.hashCode());
	}
	
	/**
	 * Creates a hash with the source, player and dice.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return (getSource().hashCode()+player+piece*10+from*100+to*1000);
	}
	
	/**
	 * Returns the player nr (piece) : from and to.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Player "+player+" ("+piece+") : "+from+"->"+to;
	}

	/**
	 * @return the player
	 */
	public int getPlayer() {
		return player;
	}

	/**
	 * @return the piece
	 */
	public int getPiece() {
		return piece;
	}

	/**
	 * @return the from
	 */
	public int getFrom() {
		return from;
	}

	/**
	 * @return the to
	 */
	public int getTo() {
		return to;
	}
}
