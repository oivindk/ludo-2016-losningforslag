package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;

/**
 * Used to give information about a throw of the dice.
 * 
 * @author Øivind Kolloen
 *
 */
@SuppressWarnings("serial")
public class DiceEvent extends EventObject {
	int player;
	int dice;

	/**
	 * Create a new DiceEvent with the given game as source. Also add the player that threw 
	 * the dice and the value of the dice thrown. 
	 *  
	 * @param ludo the game that the dice was thrown in.
	 * @param player the player that threw the dice.
	 * @param dice the value of the dice thrown.
	 */
	public DiceEvent(Ludo ludo, int player, int dice) {
		super(ludo);
		this.player = player;
		this.dice = dice;
	}

	/**
	 * Checks if two DiceEvent object refers to the same diceEvent.
	 * 
	 * @see java.lang.Object#equals(Object)
	 */
	@Override
	public boolean equals (Object o) {
		if (!(o instanceof DiceEvent)) {
			return false;
		}
		DiceEvent e = (DiceEvent)o;
		return (hashCode()==e.hashCode());
	}
	
	/**
	 * Creates a hash with the source, player and dice.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return (getSource().hashCode()+player+dice);
	}
	
	/**
	 * Returns the player nr and dice thrown.
	 * 
	 * @see java.lang.Object#toString()
	 */

	@Override
	public String toString() {
		return "Player: "+player+" -> "+dice;
	}

	/**
	 * @return the dice
	 */
	public int getDice() {
		return dice;
	}
}
