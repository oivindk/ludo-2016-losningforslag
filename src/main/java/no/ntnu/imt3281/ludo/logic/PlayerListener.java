package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Used to listen for PlayerEvents.
 * @see PlayerEvent
 * 
 * @author Øivind Kolloen
 *
 */
public interface PlayerListener extends EventListener {

	/**
	 * Called when a player changes state (gets the dice, looses the dice, wins a game, leaves a game.)
	 * 
	 * @param ple a PlayerEvent object with details about the event.
	 */
	void playerStateChanged(PlayerEvent ple);

}
