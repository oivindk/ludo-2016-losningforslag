package no.ntnu.imt3281.ludo.logic;

/**
 * This class holds the player space to ludo space lookup tables.
 * 
 * @author Øivind Kolloen
 *
 */
public class PlayerSpaceToLudoSpace {
	private int[][] userSpaceToLudoSpace = new int[4][60];
	
	/**
	 * Initialize the player space to Ludo space lookup tables.
	 */
	public PlayerSpaceToLudoSpace() {
		// Player home spaces
		userSpaceToLudoSpace[Ludo.RED][0] = 0;
		userSpaceToLudoSpace[Ludo.BLUE][0] = 4;
		userSpaceToLudoSpace[Ludo.YELLOW][0] = 8;
		userSpaceToLudoSpace[Ludo.GREEN][0] = 12;
		
		// Player start spaces
		userSpaceToLudoSpace[Ludo.RED][1] = 16;
		userSpaceToLudoSpace[Ludo.BLUE][1] = 29;
		userSpaceToLudoSpace[Ludo.YELLOW][1] = 42;
		userSpaceToLudoSpace[Ludo.GREEN][1] = 55;

		// All red spaces
		for (int i=16; i<68; i++) {
			userSpaceToLudoSpace[Ludo.RED][i-15] = i;
		}
		userSpaceToLudoSpace[Ludo.RED][53] = 16;
		for (int i=54; i<60; i++) {
			userSpaceToLudoSpace[Ludo.RED][i] = i+14;
		}
		
		// All blue spaces
		for (int i=1; i<40; i++) {
			userSpaceToLudoSpace[Ludo.BLUE][i] = i+28;
		}
		for (int i=16; i<30; i++) {
			userSpaceToLudoSpace[Ludo.BLUE][i+24] = i;
		}
		for (int i=54; i<60; i++) {
			userSpaceToLudoSpace[Ludo.BLUE][i] = i+20;
		}
		
		// All yellow spaces
		for (int i=1; i<27; i++) {
			userSpaceToLudoSpace[Ludo.YELLOW][i] = i+41;
		}
		for (int i=27; i<54; i++) {
			userSpaceToLudoSpace[Ludo.YELLOW][i] = i-11;
		}
		for (int i=54; i<60; i++) {
			userSpaceToLudoSpace[Ludo.YELLOW][i] = i+26;
		}
		
		// All green spaces
		for (int i=1; i<14; i++) {
			userSpaceToLudoSpace[Ludo.GREEN][i] = i+54;
		}
		for (int i=14; i<54; i++) {
			userSpaceToLudoSpace[Ludo.GREEN][i] = i+2;
		}
		for (int i=54; i<60; i++) {
			userSpaceToLudoSpace[Ludo.GREEN][i] = i+32;
		}
	}

	/**
	 * Given a player and this players location returns the board location.
	 * 
	 * @param player the player to look up a location for
	 * @param location the player location to look up
	 * @return the board location.
	 */
	public int getLudoLocationFromUserLocation (int player, int location) {
		return userSpaceToLudoSpace[player][location];
	}
}