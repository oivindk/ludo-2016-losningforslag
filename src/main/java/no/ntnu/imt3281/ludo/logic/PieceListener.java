package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Used to listen for piece movements on the board.
 * 
 * @author Øivind Kolloen
 *
 */
public interface PieceListener extends EventListener {

	/**
	 * This method gets called whenever a piece is moved on the board.
	 * 
	 * @param pe information about the move.
	 */
	void pieceMoved(PieceEvent pe);

}
