package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;

/**
 * Objects of this class holds all information about a game.
 * Used both on the server and on the clients.
 * 
 * @author Øivind Kolloen
 *
 */
public class Ludo {
	/** RED is the first player */
	public static final int RED = 0;
	/** BLUE is the second player */
	public static final int BLUE = 1;
	/** YELLOW is the third player */
	public static final int YELLOW = 2;
	/** GREEN is the fourth player */
	public static final int GREEN = 3;
	
	private ArrayList<String> players = new ArrayList<>();
	private int pieces[][] = new int[4][4];
	
	private int activePlayer = RED;
	private int lastDiceThrown = -1;
	private int attemptsAtSix = 0;
	private int sixesInARow = 0;
	private boolean shouldMove = false;
	
	private Random diceGenerator = new Random();
	
	private String status = "Created";
	private int winner = -1;
	private final static PlayerSpaceToLudoSpace userToLudo = new PlayerSpaceToLudoSpace();

	private ArrayList<DiceListener> diceListeners = new ArrayList<>();
	private ArrayList<PieceListener> pieceListeners = new ArrayList<>();
	private ArrayList<PlayerListener> playerListeners = new ArrayList<>();

	/**
	 * Creates a new, empty game.
	 * Use @link #addPlayer(String) for how to add players.
	 */
	public Ludo() {
		for (int player=0; player<4; player++) {
			for (int piece=0; piece<4; piece++) {
				pieces[player][piece] = 0;
			}
		}
	}	

	/**
	 * Create new game with  the given players.
	 * When creating a new game with the given players at least two players must be given.
	 * 
	 * 
	 * @param player1 name of player 1.
	 * @param player2 name of player 2.
	 * @param player3 name of player 3 or null if no such player.
	 * @param player4 name of player 4 or null if no such player.
	 * @throws NotEnoughPlayersException if not at least two players are given.
	 */
	public Ludo(String player1, String player2, String player3, String player4) {
		super();
		if (player1!=null) {
			players.add(player1);
		}
		if (player2!=null) {
			players.add(player2);
		}
		if (player3!=null) {
			players.add(player3);
		}
		if (player4!=null) {
			players.add(player4);
		}
		if (players.size()<2) {
			throw new NotEnoughPlayersException();
		}
		status = "Initiated";
	}

	/**
	 * Method used to add player to this game.
	 * 
	 * @param playerName the name of the player to add.
	 */
	public void addPlayer(String playerName) {
		if (players.size()<4) {
			players.add(playerName);
		} else {
			throw new NoRoomForMorePlayersException();
		}
		status = "Initiated";
	}

	/**
	 * Method used to get the number of players in this game.
	 * @see #activePlayers for how to get the number of active players.
	 * 
	 * @return the number of registered players in this game.
	 */
	public int nrOfPlayers() {
		return players.size();
	}

	/**
	 * Method used to get the name of a given player.
	 * 
	 * @param player the player to get the name for (RED, BLUE, YELLOW or GREEN) 
	 * @return the name of the given player.
	 */
	public String getPlayerName(int player) {
		if (players.size()>player) {
			String name = players.get(player);
			return name.startsWith("****")?"Inactive: "+name.substring(4):name;
		} else {
			return null;
		}
	}

	/**
	 * Remove player from game. Do not remove player entirely, but mark
	 * player as inactive.
	 * 
	 * @param playerName the name of the player to remove from the game.
	 */
	public void removePlayer(String playerName) {
		int idx = players.indexOf(playerName);
		players.set(idx, "****"+playerName);

		PlayerEvent pe = new PlayerEvent(this, idx, PlayerEvent.LEFTGAME);
		for (PlayerListener pl : playerListeners) {
			pl.playerStateChanged(pe);
		}		

		if (idx == activePlayer) {
			nextPlayer();
		}		
	}

	/**
	 * Get number of active players. This is the number of players 
	 * still active in the game. 
	 * @see #nrOfPlayers for how to get the number of players registered for this game.
	 * @return the number of active (still taking part in the game) players
	 */
	public int activePlayers() {
		int count = 0;
		for (String player : players) {
			if (!player.startsWith("****"))		// Players with names starting with "****" has left the game
				count++;
		}
		return count;
	}

	/**
	 * Used to get the position of a given piece for a given player.
	 * 
	 * @param player the player to get the position for.
	 * @param piece the piece of the given player to get the position for.
	 * @return the position of the given piece for the given player.
	 */
	public int getPosition(int player, int piece) {
		return pieces[player][piece];
	}

	/**
	 * Get the active player (RED, BLUE, YELLOW, GREEN)
	 * 
	 * @return an int representing the active player.
	 */
	public int activePlayer() {
		return activePlayer;
	}

	/**
	 * This method is used on the server to throw the dice for the active player.
	 * This generates a random value between 1 and 6 inclusive. 
	 * @see #throwDice(int) for the method used on the clients.
	 * 
	 * @return a value between 1 and 6 representing the value of the dice thrown.
	 */
	public int throwDice() {
		status = "Started";
		return throwDice (diceGenerator.nextInt(6)+1);
	}

	/**
	 * Set the value of the dice thrown for the active player.
	 * This method is used on the clients to set the value of the dice
	 * that the current player has thrown.
	 * @see #throwDice() for the method used on the server.
	 * 
	 * @param dice an int between 1 and 6 representing the value of the dice.
	 * @return the same as the input value
	 */
	public int throwDice(int dice) {
		DiceEvent diceEvent = new DiceEvent(this, activePlayer, dice);
		for (DiceListener listener : diceListeners) {
			listener.diceThrown(diceEvent);
		}
		status = "Started";
		lastDiceThrown = dice;
		shouldMove = true;
		if (!piecesInPlay(activePlayer)&&dice==6) {
		} else if (!piecesInPlay(activePlayer)&&attemptsAtSix<2) {
			shouldMove = false;
			attemptsAtSix++;
		} else if (!piecesInPlay(activePlayer)&&attemptsAtSix==2) {
			shouldMove = false;
			nextPlayer();
		} else if (lastDiceThrown==6&&sixesInARow<2) {
			sixesInARow++;
		} else if (lastDiceThrown==6&&sixesInARow==2) {
			shouldMove = false;
			nextPlayer();
		} else if (!canMove()&&lastDiceThrown!=6) {
			shouldMove = false;
			nextPlayer();
		} else if (allBlocked()) {
			shouldMove = false;
			nextPlayer();
		}
		return dice;
	}
	
	/**
	 * None of players pieces can be moved due to being blocked by opponents towers.
	 * 
	 * @return true if all pieces in play is blocked by towers.
	 */
	private boolean allBlocked() {
		for (int player=0; player<4; player++) {
			if (player!=activePlayer&&piecesInPlay(player)) {
				int[] locations = findTowers(player);
				if (locations!=null&&locations.length==1) {
					if (blocks(userGridToLudoBoardGrid(player, locations[0]))) {
						return true;
					}
				} else if (locations!=null) {
					if (blocks(userGridToLudoBoardGrid(player, locations[0]))||blocks(userGridToLudoBoardGrid(player, locations[1]))) {
						return true;
					}
				}
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Checks if a tower on the given board location blocks activePlayer from moving any pieces (that is, activePlayer can not move 
	 * any of his/her pieces.)
	 * 
	 * @param boardLocation the location on the board with the block.
	 * @return
	 */
	private boolean blocks(int boardLocation) {
		for (int location : pieces[activePlayer]) {
			if (location==0&&lastDiceThrown==6) {								// User has piece in start and threw 6
				if (userGridToLudoBoardGrid(activePlayer, 1)!=boardLocation)	// No block on field 1
					return false;													// Not blocked
			} else if (location>52&&location<59) {								// User has a piece on home stretch 
				return false;														// Not blocked
			} else if (location>0&&location<=53) {								// Users piece is in play
				boolean block = false;
				for (int i=1; i<=lastDiceThrown; i++) {							// Check from position and dice positions onward
					if (userGridToLudoBoardGrid(activePlayer, location+i)==boardLocation) {
						block = true;											// Block detected
					}
				}
				if (!block) {													// No block detected
					return false;
				}
			}
		}
		return true;															// If we get here then no openings was detected.
	}

	/**
	 * Find all towers (two or more pieces stacked on top of each others) that this player has.
	 * Only return towers of pieces in play (not on start or finish field.)
	 * 
	 * @param player the player to find towers from
	 * @return an array of locations with towers or null if no towers exist.
	 */
	private int[] findTowers(int player) {
		HashMap<Integer, Integer> towers = new HashMap<>();
		for (int piece = 0; piece<4; piece++) {
			if (pieces[player][piece]!=0&&pieces[player][piece]!=59) {
				if (towers.containsKey(pieces[player][piece])) {
					towers.put(pieces[player][piece], 2);
				} else { 
					towers.put(pieces[player][piece], 1);
				}
			}
		}
		Iterator<Entry<Integer, Integer>> iterator = towers.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, Integer> e = iterator.next();
			if (e.getValue()==1) {
				iterator.remove();
			}
		}
		if (towers.size()==0) {
			return null;
		}
		Integer[] tmp = towers.keySet().toArray(new Integer[towers.keySet().size()]);
		int locations[] = new int[tmp.length];
		for (int i=0; i<tmp.length; i++) {
			locations[i] = tmp[i];
		}
		return locations;
	}

	/**
	 * Checks to see if active player has any pieces in play
	 * 
	 * @return true is player has pieces in play, otherwise returns false
	 */
	private boolean piecesInPlay(int player) {
		for (int pos : pieces[player]) {
			if (pos!=0&&pos!=59) { 				// A piece is not at start or finish squares
				return true;					// means a piece is in play and not at home
			}
		}
		return false;							// No pieces in play, means all at home
	}

	/**
	 * Move players piece from one field to another. Select the first piece that the user 
	 * has on the from field and move it to the "to" field. Returns true if a piece can be moved
	 * and false if no piece could be moved. Note, this method also checks that the 
	 * difference between from and to is equal to the number on the dice.
	 * 
	 * @param player the player to move the piece for.
	 * @param from the field to move the piece from.
	 * @param to the field to move the piece to.
	 * @return true if a piece was moved, false otherwise.
	 */
	public boolean movePiece(int player, int from, int to) {
		if (lastDiceThrown==6&&to==1) {
			for (int piece=0; piece<4; piece++) {
				if (pieces[player][piece]==from&&to==1) {
					pieces[player][piece] = to;
					PieceEvent pe = new PieceEvent(this, activePlayer, piece, from, to);
					for (PieceListener pl : pieceListeners) {
						pl.pieceMoved(pe);
					}
					checkForUnfortunateOpponent(to);
					nextPlayer();
					return true;
				}
			}
		}
		if (canMove(player, from, to)) {
			for (int piece=0; piece<4; piece++) {
				if (pieces[player][piece]==from&&to==from+lastDiceThrown) {
					pieces[player][piece] = to;
					PieceEvent pe = new PieceEvent(this, activePlayer, piece, from, to);
					for (PieceListener pl : pieceListeners) {
						pl.pieceMoved(pe);
					}
					checkForUnfortunateOpponent(to);
					checkWinner(player);
					if (lastDiceThrown!=6) {
						nextPlayer();
					}
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Did the last move by activePlayer end up on one of the opponents pieces?
	 * If so, move that piece back to start.
	 * 
	 * @param location the location where the active player placed his piece.
	 */
	private void checkForUnfortunateOpponent(int location) {
		for (int player = 0; player<4; player++) {
			if (player!=activePlayer) {						// Don't check towards active player.
				for (int piece=0; piece<4; piece++) {
					if (userGridToLudoBoardGrid(activePlayer, location)==userGridToLudoBoardGrid(player, pieces[player][piece])) {
						PieceEvent pe = new PieceEvent(this, player, piece, pieces[player][piece], 0);
						pieces[player][piece] = 0;
						for (PieceListener pl : pieceListeners) {
							pl.pieceMoved(pe);
						}

					}
				}
			}
		}
	}

	/**
	 * Check to see if this player has won the game, if so, set status to finished.
	 * 
	 * @param player the player to check to see if has won.
	 */
	private void checkWinner(int player) {
		for (int pos : pieces[player]) {
			if (pos!=59) 
				return;
		}
		PlayerEvent pe = new PlayerEvent(this, player, PlayerEvent.WON);
		for (PlayerListener pl : playerListeners) {
			pl.playerStateChanged(pe);
		}
		status = "Finished";
		winner  = player;
	}

	/**
	 * Check if currentUser can move a piece given the current dice.
	 * 
	 * @return true if the current user can move a piece, false otherwise.
	 */
	private boolean canMove() {
		for (int pos : pieces[activePlayer]) {
			if (canMove(activePlayer, pos, pos+lastDiceThrown))
				return true;
		}
		return false;
	}
	
	/**
	 * Check if currentUser has thrown the dice and should move a piece before throwing again.
	 * 
	 * @return true if this user should move a piece.
	 */
	protected boolean shouldMove() {
		return shouldMove;
	}
	
	/**
	 * Used bu GameBoard to check if user can move a piece on the given location.
	 * 
	 * @param userLocation the location of the piece
	 * 
	 * @return true if the piece can be moved, false otherwise.
	 */
	protected boolean canMove(int userLocation) {
		if (lastDiceThrown==6&&userLocation==0) 
			return true;
		return canMove(activePlayer, userLocation, userLocation+lastDiceThrown);
	}
	
	/**
	 * Check to see if player can move a piece from one position to another.
	 * 
	 * @param player the player to check for
	 * @param from the from position
	 * @param to the to position
	 * @return true if the move can be performed, false otherwise
	 */
	protected boolean canMove(int player, int from, int to) {
		for (int pos : pieces[player]) {
			if (pos>0&&pos==from&&to<=59) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Sets next active player to be current player
	 */
	private void nextPlayer() {
		if (!players.get(activePlayer).startsWith("****")) {
			PlayerEvent pe = new PlayerEvent(this, activePlayer, PlayerEvent.WAITING);
			for (PlayerListener pl : playerListeners) {
				pl.playerStateChanged(pe);
			}
		}
		do {
			activePlayer++;
			activePlayer = activePlayer%nrOfPlayers();
		} while (players.get(activePlayer).startsWith("****"));
		PlayerEvent pe = new PlayerEvent(this, activePlayer, PlayerEvent.PLAYING);
		for (PlayerListener pl : playerListeners) {
			pl.playerStateChanged(pe);
		}
		attemptsAtSix = 0;
		sixesInARow = 0;
		lastDiceThrown = -1;
	}

	/**
	 * Return positions for players pieces.
	 * 
	 * @param player the player to find piece positions for.
	 * 
	 * @return an array with the location of the players pieces.
	 */
	public int[] getPlayerPieces(int player) {
		return pieces[player];
	}
	
	/**
	 * Returns the status of the game. This is returned as string with the following meaning : <ul>
	 * <li>Created - Game created but no players added</li>
	 * <li>Initiated - Game created and players added</li>
	 * <li>Started - First player (RED) has thrown the dice</li>
	 * <li>Finished - A player has finished the game</li></ul>
	 * 
	 * @return a String giving the status of the game.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Get the winner of the game (RED, BLUE, YELLOW or GREEN)
	 * 
	 * @return an int indicating the winner of the game.
	 */
	public int getWinner() {
		return winner;
	}

	/**
	 * Convert between the user location and the board location. To simplify programming
	 * all movements is done in user space, that is, moving a piece for RED from position 1 to 2
	 * and moving a piece for BLUE from position 1 to 2 involves different positions on the board
	 * but from the players point of view it is the same.
	 * 
	 * @param player the player to get the board location for
	 * @param location the location from the players point of view
	 * @return the actual location on the board.
	 */
	public int userGridToLudoBoardGrid(int player, int location) {
		return Ludo.userToLudo.getLudoLocationFromUserLocation(player, location);
	}

	/**
	 * Adds a DiceListener to this game. This will be notified whenever the dice has been thrown.
	 * 
	 * @param diceListener the listener to be added.
	 */
	public void addDiceListener(DiceListener diceListener) {
		diceListeners.add(diceListener);
	}

	/**
	 * Adds a PieceListener to this game. This will be notified whenever a pice is moved.
	 * 
	 * @param pieceListener the listener to be added.
	 */
	public void addPieceListener(PieceListener pieceListener) {
		pieceListeners.add(pieceListener);
	}

	/**
	 * Adds a PlayerListener to this game. This will be notified with player related events.
	 * I.e. when it is a players turn, when a player is done with his/her turn, when a player
	 * leaves, when a player wins the game.
	 * 
	 * @param playerListener the listener to be added.
	 */
	public void addPlayerListener(PlayerListener playerListener) {
		playerListeners .add(playerListener);
	}
}
