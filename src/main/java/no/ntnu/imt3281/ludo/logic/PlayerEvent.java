package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;

/**
 * Holds details about a playerEvent. A player event is sent when 
 * the state of a player changes.
 * 
 * @author Øivind Kolloen
 *
 */
@SuppressWarnings("serial")
public class PlayerEvent extends EventObject {
	private int player;
	private int state;
	
	/** Indicating that this player is no longer holding the dice. */
	public static final int WAITING = 1;
	/** Indicating that this player is holding the dice. */
	public static final int PLAYING = 0;
	/** Indicating that this player has left the game. */
	public static final int LEFTGAME = 2;
	/** Indicating that this player is the winner the game. */
	public static final int WON = 3;

	/**
	 * Creates a new event holding a reference to the game it originated. The
	 * event also indicates which player it relates to and the new state of this player.
	 * 
	 * @param game a reference to the game being player.
	 * @param player what player this event relates to.
	 * @param state the new state of the player.
	 */
	public PlayerEvent(Ludo game, int player, int state) {
		super(game);
		this.player = player;
		this.state = state;
	}
	
	/**
	 * Checks if two PlayerEvent object refers to the same playerEvent.
	 * 
	 * @see java.lang.Object#equals(Object)
	 */
	@Override
	public boolean equals (Object o) {
		if (!(o instanceof PlayerEvent)) {
			return false;
		}
		PlayerEvent e = (PlayerEvent)o;
		return (hashCode()==e.hashCode());
	}
	
	/**
	 * Creates a hash with the source, player and dice.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return (getSource().hashCode()+player+state*10);
	}
	
	/**
	 * Returns the player nr : state.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Player "+player+" : "+state;
	}

	/**
	 * @return the player
	 */
	public int getPlayer() {
		return player;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}
}
