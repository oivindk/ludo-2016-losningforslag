/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

/**
 * @author Øivind Kolloen
 *
 */
@SuppressWarnings("serial")
public class NotEnoughPlayersException extends RuntimeException {

}
