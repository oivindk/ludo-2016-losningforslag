package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Used to listen for dice throws in a game.
 * 
 * @author Øivind Kolloen
 *
 */
public interface DiceListener extends EventListener {

	/**
	 * Method called when a dice is thrown.
	 * 
	 * @param diceEvent details about the dice that was thrown.
	 */
	void diceThrown(DiceEvent diceEvent);

}
