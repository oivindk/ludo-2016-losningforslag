/**
 * This package includes all purely client related classes. The GUI for the client is
 * defined in the package no.ntnu.imt3281.ludo.gui
 */
/**
 * @author Øivind Kolloen
 *
 */
package no.ntnu.imt3281.ludo.client;