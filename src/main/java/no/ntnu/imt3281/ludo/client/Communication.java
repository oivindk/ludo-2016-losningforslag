package no.ntnu.imt3281.ludo.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.logging.Level;

import no.ntnu.imt3281.io.Tools;
import no.ntnu.imt3281.logging.Logger;

/**
 * All communication with the server goes through this class.
 * Provides all client classes one way to get access to the DatagramSocket.
 * 
 * @author Øivind Kolloen
 *
 */
public class Communication {
	private InetSocketAddress serverAddress;
	private DatagramSocket socketConnection;
	private static final Communication connection = new Communication();

	/**
	 * Create DatagramSocket to communicate with server.
	 * Server address and port number is taken from /ludo.config file if it exists.
	 * Defined as private, meaning no other classes can create an instance of this object.
	 */
	private Communication() {
		serverAddress = new InetSocketAddress(LudoProperties.getProperties().getProperty("serverAddress", "127.0"+".0.1"), Integer.parseInt(LudoProperties.getProperties().getProperty("serverPort", "6789")));
		try {
			socketConnection = new DatagramSocket();
		} catch (SocketException e) {
			Logger.getLogger().log(Level.SEVERE, "Unable to open datagram socket", e);
			System.exit(-1);
		}
	}

	/**
	 * There will only be one Communication object in an application, use this method to get
	 * a reference to this object.
	 *  
	 * @return a reference to the Communication object.
	 */
	public static Communication getConnection() {
		return connection;
	}
	
	/**
	 * Send an object to the server.
	 * This is a convenience method since this the most used functionality.
	 * 
	 * @param object the object to send
	 * @return true if object sent without error, false otherwise.
	 */
	public boolean send (Object object) {
		byte buffer[];
		try {
			buffer = Tools.convertToBytes(object);
    			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, serverAddress);
    			socketConnection.send(packet);
		} catch (IOException e) {
			Logger.getLogger().log(Level.WARNING, "Error sending object", e);
			return false;
		}
		return true;
	}
	
	/**
	 * Returns the DatagramSocket for this connection.
	 * 
	 * @return the DatagramSocket for this connection.
	 */
	public DatagramSocket getSocket() {
		return socketConnection;
	}
}
