package no.ntnu.imt3281.ludo.client;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;

import no.ntnu.imt3281.logging.Logger;

/**
 * Used to access properties in the Ludo application.
 * Works system wide and only creates one instance of the properties object.
 * 
 * @author okolloen
 *
 */
public class LudoProperties {
	private  Properties properties = new Properties();
	private static final LudoProperties ludoProperties = new LudoProperties();
	
	/**
	 * Private constructor prevents anyone else from instantiating this class.
	 */
	private LudoProperties() {
		try {
			InputStream is = new FileInputStream("./ludo.config");	// the config file should be placed with the jar file.
			properties.load(is);
			is.close();
		} catch (IOException e) {
			Logger.getLogger().log(Level.INFO, "Unable to open ludo.config", e);
		}
	}
	
	/**
	 * Returns an instance of the properties object.
	 * 
	 * @return an instance of the properties object for the Ludo application
	 */
	static public Properties getProperties() {
		return LudoProperties.ludoProperties.properties;
	}
	
	/**
	 * Stores the properties to disc. 
	 */
	static public void storeProperties() {
		try {
			OutputStream is = new FileOutputStream("./ludo.config");
			LudoProperties.ludoProperties.properties.store(is, "Ludo properties file");
			is.close();
		} catch (IOException e) {
			Logger.getLogger().log(Level.INFO, "Unable to open ludo.config", e);
		}
	}
}