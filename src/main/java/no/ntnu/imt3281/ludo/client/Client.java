package no.ntnu.imt3281.ludo.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.io.Tools;
import no.ntnu.imt3281.io.dataPackets.ChannelInfo;
import no.ntnu.imt3281.io.dataPackets.ChatMessage;
import no.ntnu.imt3281.io.dataPackets.ChatParticipant;
import no.ntnu.imt3281.io.dataPackets.ListRooms;
import no.ntnu.imt3281.io.dataPackets.LoggedIn;
import no.ntnu.imt3281.io.dataPackets.MovePiece;
import no.ntnu.imt3281.io.dataPackets.Ping;
import no.ntnu.imt3281.io.dataPackets.PlayerLeftGame;
import no.ntnu.imt3281.io.dataPackets.Pong;
import no.ntnu.imt3281.io.dataPackets.RandomGameRequestUpdate;
import no.ntnu.imt3281.io.dataPackets.StartGame;
import no.ntnu.imt3281.io.dataPackets.ThrowDice;
import no.ntnu.imt3281.logging.Logger;
import no.ntnu.imt3281.ludo.gui.Ludo;

/**
 * 
 * This is the main class for the client. 
 * Reads no.ntnu.imt3281.gui.Ludo.fxml and sets it up as the main
 * GUI for the application. The controller from Ludo.fxml (Ludo) handles
 * all menu and toolbar interactions.
 * A DatagramSocket is set up in this class that receives all messages from the 
 * server and sends them on to the Ludo controler.
 * 
 * @author Øivind Kolloen 
 *
 */
public class Client extends Application {
	private Window mainWindow;
	private Ludo ludo;
	private boolean stopping = false;
	private String token = null;
	private Stage primaryStage;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../gui/Ludo.fxml"));
	    		loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

			BorderPane root = (BorderPane)loader.load();
			ludo = loader.getController();
			Scene scene = new Scene(root);
			mainWindow = scene.getWindow();
			ludo.setMainWindow(mainWindow);
			primaryStage.setScene(scene);
			primaryStage.setTitle(I18N.getString("mainWindow.title"));
			primaryStage.show();
			this.primaryStage = primaryStage;
		} catch(Exception e) {
			Logger.getLogger().log(Level.SEVERE, "Unable to load main window definition", e);
			System.exit(-2);
		}
		ExecutorService pool = Executors.newCachedThreadPool();
		pool.execute(()-> inputListener());		// Receives messages from the server
	}
	
	@Override
	public void stop() {
		LudoProperties.storeProperties();
		System.exit(0);
	}

	/**
	 * This method runs in a separate thread and receives all messages from the server.
	 * The datagram socket is set up via the {@link Communication} class.
	 */
	private void inputListener() {
		Communication connection = Communication.getConnection();
		while (!stopping) {
			byte[] buffer = new byte[1024*64];			// Max datapacket size
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			try {
				connection.getSocket().receive(packet);	// Wait for packet
				Object o = Tools.convertFromBytes(packet.getData());	// Convert from array of bytes to object
				
				// Check to see what object was actually sent from the server
				if (o instanceof LoggedIn) {				// User was logged in
					token = ((LoggedIn)o).getToken();
					LudoProperties.getProperties().put("token", token);
					ludo.createMasterChat();
					String uname[] = { LudoProperties.getProperties().getProperty("username") };
					Platform.runLater(()->primaryStage.setTitle(I18N.getConcatenatedString("mainWindow.title.loggedIn", uname)));
				} else if (o instanceof ChatMessage) {	// Received a chat message
					ludo.chatMessage((ChatMessage)o);
				} else if (o instanceof ChannelInfo) {	// Joined a new channel
					ludo.createChat((ChannelInfo)o);
				} else if (o instanceof ChatParticipant) {	// A person joined/left a chat channel
					ludo.enterLeaveChannel((ChatParticipant)o);
				} else if (o instanceof PlayerLeftGame) { 	// A player left a game  
					ludo.playerLeftGame((PlayerLeftGame)o);
				} else if (o instanceof StartGame) {		// A new game has started
					ludo.startNewGame((StartGame)o);
				} else if (o instanceof ThrowDice) { 	// A Dice has been trown in a game
					ludo.diceThrown((ThrowDice)o);
				} else if (o instanceof MovePiece) { 	// A piece has been moved in a game
					ludo.movePiece((MovePiece)o);
				} else if (o instanceof ListRooms) { 	// Received a list of all rooms
					Platform.runLater(()->ludo.roomListReceived((ListRooms)o));
				} else if (o instanceof Ping) {			// Received a ping, should respond with pong
					Pong pong = new Pong(token);
					connection.send(pong);
				} else if (o instanceof RandomGameRequestUpdate) {	// I am waiting to join a random game and got a status update
					ludo.randomGameUpdate((RandomGameRequestUpdate)o);
				}
			} catch (IOException e) {	// Any IOException would probably mean we are no longer connected to the network
				Logger.getLogger().warning("Unable to send to client : "+
						connection.getSocket().getRemoteSocketAddress().toString()+
						" -> "+e.getMessage());
			} catch (no.ntnu.imt3281.io.ObjectConversionException e) {	// Time to update the client
				Logger.getLogger().warning("Unable to handle received data : "+e.getMessage());
			}
		}
	}
	
	/**
	 * The one and only :-). Starts the client
	 * @param args send to {@link Application#launch(String...)}
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
