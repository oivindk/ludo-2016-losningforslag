package no.ntnu.imt3281.ludo.gui;

import java.awt.event.ActionListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.imt3281.io.dataPackets.Login;
import no.ntnu.imt3281.ludo.client.LudoProperties;

/**
 * Used for entering username/password for login
 * 
 * @author oivindk
 *
 */
public class LoginDialog {
	private ActionListener al;
    private Stage dialog;

    @FXML private Button cancel;
    @FXML private PasswordField pwd;
    @FXML private Button ok;
    @FXML private TextField username;

    /**
     * Need access to the stage to be able to close the dialog
     * 
     * @param dialog the stage for the dialog
     */
    public void setStage (Stage dialog) {
    		this.dialog = dialog;
    }
    
    /**
     * Called when user presses the ok button in the dialog (or presses enter
     * in one of the field.)
     * Calls the registered actionListener to enable the code using the dialog
     * to get the username/password values from this dialog.
     * 
     * @param event not used
     */
    @FXML
    void okPressed(ActionEvent event) {
    		// The calling code will use the "this" reference to access username/password
       	al.actionPerformed(new java.awt.event.ActionEvent(this, 0, ""));
       	dialog.hide();
    }
    
    /**
     * Set method to be called (ActionListener.actionPerformed) when OK button is pressed.
     * 
     * @param al the actionListener that has the actionPerformed method.
     */
	public void setOKHandler(ActionListener al) {
		this.al = al;
	}
	
	/**
	 * Returns data about new user, only called when user presses OK button
	 * 
	 * @return and Login object with username and password
	 */
	public Login getLogindata() {
		LudoProperties.getProperties().put("username", username.getText());
		return new Login (username.getText(), pwd.getText());
	}

	/**
	 * Closes the dialog when cancel is pressed.
	 * 
	 * @param event not used
	 */
    @FXML
    void cancelPressed(ActionEvent event) {
    		dialog.hide();
    }
}