/**
 * This package includes all purely gui related classes/resources
 */
/**
 * @author Øivind Kolloen 
 *
 */
package no.ntnu.imt3281.ludo.gui;