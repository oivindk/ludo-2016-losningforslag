package no.ntnu.imt3281.ludo.gui;

import java.awt.Point;
import javafx.application.Platform;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.io.dataPackets.MovePiece;
import no.ntnu.imt3281.io.dataPackets.StartGame;
import no.ntnu.imt3281.io.dataPackets.ThrowDice;
import no.ntnu.imt3281.ludo.client.Communication;
import no.ntnu.imt3281.ludo.client.LudoProperties;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;

/**
 * Objects of this class represents a game of ludo on the client.
 * Inherits from {@link no.ntnu.imt3281.ludo.logic.Ludo} that contains all logic 
 * for the ludo game. Act as controller for GameBoard.fxml. 
 * All visual appearance and user interaction is controlled by this class.
 * 
 * @author oivindk
 *
 */
public class GameBoard extends Ludo {
    @FXML private Label player1Name;
    @FXML private Label player2Name;
    @FXML private Label player3Name;
    @FXML private Label player4Name;
    @FXML private ImageView player1Active;
    @FXML private ImageView player2Active;
    @FXML private ImageView player3Active;
    @FXML private ImageView player4Active;
    @FXML private ImageView diceThrown;
    @FXML private Button throwTheDice;
    @FXML private TextArea chatArea;
    @FXML private TextField textToSay;
    @FXML private Button sendTextButton;
    @FXML private AnchorPane boardPane;
    @FXML private ImageView board;
    private int IAmPlayer = RED;
    private int gameId;
    private Communication connection = Communication.getConnection();
    private Image playerPieceImages[] = new Image[4];
    private Rectangle moveFrom = new Rectangle(46, 46);
    private Rectangle moveTo = new Rectangle(46, 46);
    private Rectangle playerPieces[][] = new Rectangle[4][4];
    private TopLeftCorners corners = new TopLeftCorners();
    private boolean shouldMove = false;
    private int moveUserPieceFrom = -1;
    private int diceValue = -1;
    
    /**
     * Sets up players for this game.
     * 
     * @param gameInfo contains names of players for this game.
     */
    public void initializeGame(StartGame gameInfo) {
	    	this.gameId = gameInfo.getGameCode();
	    	for (String playerName : gameInfo.getPlayerNames()) {
	    		addPlayer(playerName);
	    	}
	    	player1Name.setText(getPlayerName(RED));
	    	player2Name.setText(getPlayerName(BLUE));
	    	player3Name.setText(getPlayerName(YELLOW));
	    	player4Name.setText(getPlayerName(GREEN));
	    	
	    	// Figure out what player this user is
	    	if (LudoProperties.getProperties().getProperty("username").equals(getPlayerName(BLUE)))
	    		IAmPlayer = BLUE;
	    	if (LudoProperties.getProperties().getProperty("username").equals(getPlayerName(YELLOW)))
	    		IAmPlayer = YELLOW;
	    	if (LudoProperties.getProperties().getProperty("username").equals(getPlayerName(GREEN)))
	    		IAmPlayer = GREEN;
	    	if (IAmPlayer==activePlayer())
	    		throwTheDice.setDisable(false);
	    	addDiceListener(de->		// Update the dice image when the dice was thrown
	    		Platform.runLater(()-> 
	    			diceThrown.setImage(new Image(getClass().getResourceAsStream("/images/dice"+de.getDice()+".png")))
	    		)
	    	);
	    	addPlayerListener(pe->{	// Update active player display
	    		Platform.runLater(()->playerChange(pe.getPlayer(), pe.getState()));
	    	});
	    	playerPieceImages[0] = new Image(getClass().getResourceAsStream("/images/red.png"));
	    	playerPieceImages[1] = new Image(getClass().getResourceAsStream("/images/blue.png"));
	    	playerPieceImages[2] = new Image(getClass().getResourceAsStream("/images/yellow.png"));
	    	playerPieceImages[3] = new Image(getClass().getResourceAsStream("/images/green.png"));
	    	
	    	for (int i=0; i<4; i++) {	// Set up pieces for all players
	    		for (int ii=0; ii<4; ii++) {
	    			playerPieces[i][ii] = new Rectangle(48, 48);
	    			playerPieces[i][ii].setFill(new ImagePattern(playerPieceImages[i]));
	    			playerPieces[i][ii].setX(corners.point[i*4+ii].getX()-8+ii*4);
	    			playerPieces[i][ii].setY(corners.point[i*4+ii].getY()-2+ii*2);
	    			playerPieces[i][ii].setOnMouseClicked(e->clickOnPiece(e));
	    	    		boardPane.getChildren().add(playerPieces[i][ii]);
	    		}
	    	}
	    	
	    	// Set up tiles used for showing selected piece and target square
	    	moveFrom.setFill(new ImagePattern(new Image(getClass().getResourceAsStream("/images/selectedGrid.png"))));
	    	moveFrom.setX(-100);
	    	moveFrom.setY(-100);
	    	boardPane.getChildren().add(moveFrom);
	    	moveTo.setFill(new ImagePattern(new Image(getClass().getResourceAsStream("/images/selectedGrid.png"))));
	    	moveTo.setX(-100);
	    	moveTo.setY(-100);
	    	boardPane.getChildren().add(moveTo);
	    	moveTo.setOnMouseClicked(e->movePiece(e));
    }
    
    /**
     * Called when the user presses the "Throw dice" button
     * 
     * @param ae not used
     */
	@FXML
    void throwDice(ActionEvent ae) {
		connection.send(new ThrowDice(gameId));
    }

	/**
	 * Moves all the pieces to the correct positions. 
	 */
	public void updateBoard() {
		Platform.runLater(()-> {		// Do all GUI operations on the GUI thread
			if (IAmPlayer==activePlayer()) {
				throwTheDice.setDisable(false);
			}
			moveFrom.setX(-100);		// Place markings for moving pieces away from the board
		    	moveFrom.setY(-100);
		    	moveTo.setX(-100);
		    	moveTo.setY(-100);
			throwTheDice.setText(I18N.getString("ludogameboard.throwDiceButton"));
			// Update pieces on game board.
			for (int player=0; player<4; player++) {		// Place all pieces on correct positions
				int pieces[] = getPlayerPieces(player);
				int piecesHome = 0;
				for (int piece=0; piece<4; piece++) {
					playerPieces[player][piece].setX(corners.point[userGridToLudoBoardGrid(player, pieces[piece])+piecesHome].getX()-8+piece*4);
					playerPieces[player][piece].setY(corners.point[userGridToLudoBoardGrid(player, pieces[piece])+piecesHome].getY()-2+piece*2);
					if (pieces[piece]==0)
						piecesHome++;
				}
			}
		});
	}
	
	/**
	 * Called when message with dice value is received from server.
	 * Checks if player is active user and if so if this dice value means the user should move 
	 * one of his/her pieces.
	 * 
	 * @param dice the value of the dice generated on the server.
	 */
	@Override
    public int throwDice(int dice) {
	    	super.throwDice(dice);
	    	shouldMove = false;
	    	if (IAmPlayer==activePlayer()&&shouldMove()) {	// This is the player throwing the dice and a piece should be moved
    			diceValue = dice;
    			shouldMove = true;
	    		Platform.runLater(()-> {		// If user should move a piece, disable the throw dice button
	    			throwTheDice.setText(I18N.getString("ludogameboard.throwDiceButton.moveAPiece"));
	    			throwTheDice.setDisable(true);
	    		});
	    	}
	    	return dice;
    }
    
    /**
     * Called when user clicks on one of the pieces.
     * Calls {@link GameBoard#movePieceFrom(MouseEvent)} to find the piece
     * that the user clicked on. If this piece can be moved, place a marker on the
     * square where the piece can be moved. If the piece can not be moved, removed the marking 
     * done on the piece.
     * 
     * @param event contains information about what piece the user clicked on
     */
    @FXML
    void clickOnPiece(MouseEvent event) {
	    	if (shouldMove) {	// Only enable this if user should be moving a piece.
	    		moveUserPieceFrom = movePieceFrom(event);
	    		if (moveUserPieceFrom>-1) {	// Clicked on active player piece
	    			if (canMove(moveUserPieceFrom)) {	// Piece can be moved
	    				if (moveUserPieceFrom==0) {		// From start field
	    					moveTo.setX(corners.point[userGridToLudoBoardGrid(IAmPlayer, 1)].getX());
	    					moveTo.setY(corners.point[userGridToLudoBoardGrid(IAmPlayer, 1)].getY());
	    				} else {							// From somewhere on the board
	    					moveTo.setX(corners.point[userGridToLudoBoardGrid(IAmPlayer, moveUserPieceFrom+diceValue)].getX());
	    					moveTo.setY(corners.point[userGridToLudoBoardGrid(IAmPlayer, moveUserPieceFrom+diceValue)].getY());    					
	    				}
	    			} else {					// Piece can not be moved, remove marking from piece.
	    				moveFrom.setX(-100);
	    				moveFrom.setY(-100);
	    			}
	    		}
	    	}
    }

    /**
     * Called when user has indicated he/she wants to move a piece.
     * If so, moveUserPieceFrom contains the from position, and diceValue contains
     * the number of fields to move. Sends the move to the server using a MovePiece object.
     * 
     * @param e not used.
     */
    private void movePiece(MouseEvent e) {
    	connection.send(new MovePiece(gameId, IAmPlayer, moveUserPieceFrom, moveUserPieceFrom==0&&diceValue==6?1:moveUserPieceFrom+diceValue));
	}
    
	/**
	 * Check to see if the user clicked on one of the squares containing his/her own piece(s).
	 * If so, show marking that this piece is selected.
	 * 
	 * @param event contains the information about where the user clicked.
	 */
	private int movePieceFrom(MouseEvent event) {
		int retVal = -1;
		int x = (int) event.getX();
		int y = (int) event.getY();
		Object o = event.getSource();
		for (int i=0; i<4; i++) {	// Check all players pieces
			if (o.equals(playerPieces[IAmPlayer][i])) {	// Clicked on players piece
				int boardPos = userGridToLudoBoardGrid(IAmPlayer, getPlayerPieces(IAmPlayer)[i]);
				int offset = 0;
				if (getPlayerPieces(IAmPlayer)[i]==0) {
					offset = i;
				}
				retVal = getPlayerPieces(IAmPlayer)[i];
				moveFrom.setX(corners.point[boardPos+offset].getX());
				moveFrom.setY(corners.point[boardPos+offset].getY());
			}
		}
		return retVal;
	}
    
	/**
	 * Called when a {@link PlayerEvent} is received from via the playerListener.
	 * Will be called whenever a player state changes (PLAYING, WAITING, LEFTGAME, WON)
	 * 
	 * @param player the player that changed state.
	 * @param state the new state of the player.
	 */
	public void playerChange(int player, int state) {
		switch (state) {		// What state are we changing to
		case PlayerEvent.WAITING: {
			changePlayerState(player, true);
			break;
		}
		case PlayerEvent.PLAYING: {
			diceThrown.setImage(new Image(getClass().getResourceAsStream("/images/rolldice.png")));
			changePlayerState(player, false);
			break;			
		}
		case PlayerEvent.LEFTGAME: {
			switch (player) {	// What player left the game (set and (X) icon on that player)
			case RED: 
				player1Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
				break;
			case BLUE: 
				player1Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
				break;
			case YELLOW: 
				player1Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
				break;
			case GREEN: 
				player1Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
				break;
			}
			break;
		}
		case PlayerEvent.WON: {
			throwTheDice.setDisable(true);
			diceThrown.setImage(new Image(getClass().getResourceAsStream("/images/rolldice.png")));
			switch (player) {	// Give the player that won a crown :-)
			case RED: 			
				player1Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
				player1Active.setVisible(true);
				break;
			case BLUE: 
				player2Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
				player2Active.setVisible(true);
				break;
			case YELLOW: 
				player3Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
				player3Active.setVisible(true);
				break;
			case GREEN: 
				player4Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
				player4Active.setVisible(true);
				break;
			}
			break;			
		}
		}
	}

	/**
	 * Enable/disable the throwTheDice button and show the active player indication 
	 * for the active player.
	 * 
	 * @param player what player do we set the state for
	 * @param state what is the new state
	 */
	private void changePlayerState(int player, boolean state) {
		if (player==IAmPlayer)
			throwTheDice.setDisable(state);
		if (player==IAmPlayer&&!state) {
			throwTheDice.requestFocus();
		}
		switch (player) {
		case RED: player1Active.setVisible(!state);
		break;
		case BLUE: player2Active.setVisible(!state);
		break;
		case YELLOW: player3Active.setVisible(!state);
		break;
		case GREEN: player4Active.setVisible(!state);
		break;
		}
	}
	
	/**
	 * Set up and array with the top/left corner of all player squares on the board.
	 * 
	 * @author oivindk
	 *
	 */
	class TopLeftCorners {
		Point point[] = new Point[92];
		
		/**
		 * Set all locations.
		 */
		public TopLeftCorners() {
			for (int i=0; i<point.length; i++) {
				point[i] = new Point();
			}
			// RED start fields
			point[0].setLocation(554, 74);
			point[1].setLocation(554+48, 74+48);
			point[2].setLocation(554, 74+48*2);
			point[3].setLocation(554-48, 74+48);
			
			// Blue start fields
			point[4].setLocation(554, 506);
			point[5].setLocation(554+48, 506+48);
			point[6].setLocation(554, 506+48*2);
			point[7].setLocation(554-48, 506+48);

			// Yellow start fields
			point[8].setLocation(122, 506);
		    	point[9].setLocation(122+48, 506+48);
		    	point[10].setLocation(122, 506+48*2);
		    	point[11].setLocation(122-48, 506+48);
		    	
		    	// Green start fields
		    	point[12].setLocation(122, 74);
		    	point[13].setLocation(122+48, 74+48);
		    	point[14].setLocation(122, 74+48*2);
		    	point[15].setLocation(122-48, 74+48);
		    	
		    	// Board 16-67
		    	for (int i=0; i<5; i++) {
		    		point[16+i].setLocation(386, 50+i*48);
		    	}
		    	
		    	// Around blue
		    	for (int i=0; i<6; i++) {
		    		point[21+i].setLocation(434+i*48, 290);
		    	}
	    		point[27].setLocation(674, 338);
		    	for (int i=0; i<6; i++) {
		    		point[28+i].setLocation(674-i*48, 386);
		    	}
		    	
		    	// Around Yellow
		    	for (int i=0; i<6; i++) {
		    		point[34+i].setLocation(386, 434+i*48);
		    	}
	    		point[40].setLocation(338, 674);
		    	for (int i=0; i<6; i++) {
		    		point[41+i].setLocation(290, 674-i*48);
		    	}
		    	
		    	// Around green
		    	for (int i=0; i<6; i++) {
		    		point[47+i].setLocation(242-i*48, 386);
		    	}
	    		point[53].setLocation(2, 338);
		    	for (int i=0; i<6; i++) {
		    		point[54+i].setLocation(2+i*48, 290);
		    	}
		    	
		    	// back on red
		    	for (int i=0; i<6; i++) {
		    		point[60+i].setLocation(290, 242-i*48);
		    	}
		    	for (int i=0; i<2; i++) {
		    		point[66+i].setLocation(338+i*48, 2);
		    	}
		    	// Home stretch
		    	for (int i=0; i<6; i++) {
		    		point[68+i].setLocation(338, 50+i*48);
		    		point[74+i].setLocation(626-i*48, 338);
		    		point[80+i].setLocation(338, 626-i*48);
		    		point[86+i].setLocation(50+i*48, 338);    		
		    	}
		    	
		    	// Goal fields
		    	point[73].setLocation(338, 292);
		    	point[79].setLocation(384, 338);
		    	point[85].setLocation(338, 384);
		    	point[91].setLocation(292, 338);
		}
	}
}