package no.ntnu.imt3281.ludo.gui;

import java.awt.event.ActionListener;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.imt3281.io.dataPackets.CreateUser;
import no.ntnu.imt3281.ludo.client.LudoProperties;

/**
 * Controller for CreateUserDialog.fxml, used to show a dialog to the user when creating a new user.
 * Enables the user to enter all details needed to create a new user.
 * 
 * @author oivindk
 *
 */
public class CreateUserDialog {
	private ActionListener al;
    private Stage dialog;

    @FXML private Button cancel;
    @FXML private PasswordField pwd1;
    @FXML private TextField email1;
    @FXML private TextField uname;
    @FXML private TextField givenname;
    @FXML private TextField surename;
    @FXML private Button ok;
    @FXML private PasswordField pwd;
    @FXML private TextField email;
    
    /**
     * Need access to this to be able to close the dialog when OK/Cancel buttons
     * get pressed.
     * 
     * @param dialog the {@link Stage} where this content lives.
     */
    public void setStage (Stage dialog) {
    		this.dialog = dialog;
    }
    
    /**
     * Called when cancel button is pressed, closes the dialog window.
     * 
     * @param ae not used
     */
    @FXML
    private void cancelPressed (ActionEvent ae) {
    		dialog.hide();
    }

    /**
     * Called when ok button is pressed, calls the registered actionlisteners
     * actionperformed method sending a reference to this as parameter.
     * This enables the controlling object to retrieve data about the 
     * user to create.
     * 
     * @param ae not used.
     */
    @FXML
    private void okPressed (ActionEvent ae) {
	    	al.actionPerformed(new java.awt.event.ActionEvent(this, 0, ""));
	    	dialog.hide();
    }
    
    /**
     * Set method to be called (ActionListener.actionPerformed) when OK button is pressed.
     * 
     * @param al the actionListener that has the actionPerformed method.
     */
	public void setOKHandler(ActionListener al) {
		this.al = al;
	}
	
	/**
	 * Returns data about new user, only called when user presses OK button
	 * 
	 * @return and CreateUser object with username, password, givenname, surename and email.
	 */
	public CreateUser getNewUserdata() {
		LudoProperties.getProperties().put("username", uname.getText());
		return new CreateUser (uname.getText(), pwd.getText(), givenname.getText(), surename.getText(), email.getText());
	}
}
