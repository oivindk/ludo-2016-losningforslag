package no.ntnu.imt3281.ludo.gui;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.io.dataPackets.CancelRandomGame;
import no.ntnu.imt3281.io.dataPackets.JoinRandomGame;
import no.ntnu.imt3281.io.dataPackets.RandomGameRequestUpdate;
import no.ntnu.imt3281.ludo.client.Communication;
import no.ntnu.imt3281.ludo.client.LudoProperties;

/**
 * This dialog is shown when the user want to join a random game.
 * The user must confirm the desire to join a random game by pressing a button
 * and then the user can follow the progress of creating the game in this dialog. 
 * 
 * @author oivindk
 *
 */
public class RandomGameDialog {
    @FXML private Button cancel;
    @FXML private Label playersJoined;
    @FXML private Button joinRandom;
    @FXML private ProgressBar timerProgress;
    @FXML private Label timeToStart;
	private Stage dialog;
	private Communication connection = Communication.getConnection();

	/**
	 * Need access to the stage to be able to close the dialog when it is no longer wanted.
	 * 
	 * @param dialog the state for this dialog
	 */
    public void setStage (Stage dialog) {
    		this.dialog = dialog;
    }
    
    /**
     * Dialog uses concatenated string as label text, this is not supported by built in
     * I18N support in javaFX so needs to be fixed runtime.
     */
    void fixI18N() {
	    	Integer tmp[] = { 0 };
	    	playersJoined.setText(I18N.getConcatenatedString("randomGameDialog.xPlayersHaveJoined", tmp));
	    	timeToStart.setText(I18N.getConcatenatedString("randomGameDialog.startingIn", tmp));
    }
    
    /**
     * Called when user clicks on button to confirm desire to join a random game. Sends a {@link JoinRandomGame}
     * object to the server with the users token.
     * 
     * @param event not used
     */
    @FXML
    void joinRandomGamePressed(ActionEvent event) {
	    	connection.send(new JoinRandomGame(LudoProperties.getProperties().getProperty("token")));
	    	cancel.setDisable(false);
	    	joinRandom.setDisable(true);
    }
    
    /** 
     * Called when the user clicks the cancel button, sends a {@link CancelRandomGame} object to 
     * the server and closes the dialog.
     * 
     * @param event not used
     */
    @FXML
    void cancelPressed(ActionEvent event) {
	    	connection.send(new CancelRandomGame(LudoProperties.getProperties().getProperty("token")));
	    	dialog.hide();
    }

    /**
     * Received from the server when the server informs about progress in creating a random game.
     * 
     * @param update information about number of players in queue and remaining time until the 
     * game will be started
     * 
     */
	public void update(RandomGameRequestUpdate update) {
		Integer players[] = { update.getPlayersRegistered()-1 };
		playersJoined.setText(I18N.getConcatenatedString("randomGameDialog.xPlayersHaveJoined", players));
		Integer time[] = { update.getTimeRemaining() };
		timeToStart.setText(I18N.getConcatenatedString("randomGameDialog.startingIn", time));
		if (update.getTimeRemaining()==0) {
			dialog.hide();
		}
	}
}