package no.ntnu.imt3281.ludo.gui;

import java.util.Collections;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import no.ntnu.imt3281.io.dataPackets.ChannelInfo;
import no.ntnu.imt3281.io.dataPackets.ChatMessage;
import no.ntnu.imt3281.io.dataPackets.ChatParticipant;
import no.ntnu.imt3281.io.dataPackets.GameChallenge;
import no.ntnu.imt3281.io.dataPackets.JoinChannel;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.client.Communication;
import no.ntnu.imt3281.ludo.client.LudoProperties;

/**
 * This is the controller for Chat.fxml and is used to represent a chat tab in the program.
 * Each chat tab (or channel) represents a single chat channel, messages will be received
 * from the {@link no.ntnu.imt3281.ludo.client.Client} class through the {@link Ludo} class.
 * 
 * Messages are sent directly back to the server through the {@link no.ntnu.imt3281.ludo.client.Communication} class.
 * @author oivindk
 *
 */
public class Chat {
	Communication connection = Communication.getConnection();
    @FXML private Button sayButton;
    @FXML private TextArea chatArea;
    @FXML private TextField sayText;
    @FXML private ListView<String> users;
    @FXML private ContextMenu menu;
    ObservableList<String> userList;
	private String channelName;
    
	/**
	 * Sets up the user list for use, create an observableList and set it as source for 
	 * the users ListView component and enables multiple selection on the user list.
	 */
	@FXML
    public void initialize() {
	    	userList = FXCollections.observableArrayList ();
	    	users.setItems(userList);
	    	users.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

	/**
	 * Set up this channel window with channel name and name of chat participants.
	 * 
	 * @param channelInfo object containing information about the channel.
	 */
	public void setChannelInfo(ChannelInfo channelInfo) {
		channelName = channelInfo.getChannelName();
		Collections.sort(channelInfo.getUsers());
		for (String user : channelInfo.getUsers())
			userList.add(user);		
	}

	/**
	 * Triggered when the user has selected opponent(s) from the list of users and 
	 * selected to challenge them to a game from the right click menu.
	 * Will send a message to the server using {@link Communication.send} with this
	 * users token and the name of the players.
	 *  
	 * @param event no used
	 */
    @FXML
    void challengePlayers(ActionEvent event) {
    		ObservableList<String> challengedPlayers = users.getSelectionModel().getSelectedItems();
    		String[] players = challengedPlayers.toArray(new String[challengedPlayers.size()]);
    		connection.send(new GameChallenge((String) LudoProperties.getProperties().get("token"), players));
    }
	
    /**
     * Triggered when the user presses enter in the text field or pushes the button to 
     * send a message.
     * If the text starts with /join a {@link JoinChannel} object will be sent, otherwise a 
     * {@link ChatMessage} object will be sent. Either way the objects get sent by {@link Communication#send(Object)}.
     * 
     * @param ae not used
     */
	@FXML
	public void say(ActionEvent ae) {
		if (sayText.getText().startsWith("/join ")) {
    		String channelName = sayText.getText().substring(6);
    		connection.send(new JoinChannel(LudoProperties.getProperties().getProperty("token"), channelName));
    	} else {
    		connection.send(new ChatMessage(sayText.getText(), LudoProperties.getProperties().getProperty("token"), null, channelName));
    	}
    	sayText.setText("");
	}
	
	/**
	 * Add a new message to this chat.
	 * 
	 * @param message an {@link ChatMessage} object containing the message to add.
	 */
	public void newMessage(ChatMessage message) {
		Platform.runLater(()->{
			chatArea.setText(chatArea.getText()+message.getUsername()+": "+message.getMessage()+"\n");
		});
	}

	/**
	 * Called from {@link Client} by way of {@link Ludo} when a {@link ChatParticipant} object 
	 * is received from the server
	 * 
	 * @param cp information about the user entering or leaving.
	 */
	public void enterLeaveChannel(ChatParticipant cp) {
		if (cp.getAction()==ChatParticipant.LEAVE) {
			userList.remove(cp.getUserName());
		} else {
			userList.add(cp.getUserName());
			Collections.sort(userList);
		}
	}
}
