package no.ntnu.imt3281.ludo.gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import no.ntnu.imt3281.io.dataPackets.ChatMessage;
import no.ntnu.imt3281.io.dataPackets.JoinChannel;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.client.Communication;
import no.ntnu.imt3281.ludo.client.LudoProperties;

/**
 * Defines the content of the master chat pane.
 * A master chat is opened when a user logs in to the server and remains open while the user
 * is logged in.
 * 
 * @author oivindk
 *
 */
public class MasterChat {
	Communication connection = Communication.getConnection();
	
    @FXML private TextArea chatArea;
    @FXML private TextField sayText;
    @FXML private Button sayButton;

    /**
     * Called when the user presses enter in the text field or presses the button to say something
     * Sends the message entered to the server and clears the message text field.
     * 
     * @param event no used
     */
    @FXML
    public void say(ActionEvent event) {
    	if (sayText.getText().startsWith("/join ")) {
    		String channelName = sayText.getText().substring(6);
    		connection.send(new JoinChannel(LudoProperties.getProperties().getProperty("token"), channelName));
    	} else {
    		connection.send(new ChatMessage(sayText.getText(), LudoProperties.getProperties().getProperty("token")));
    	}
    	sayText.setText("");
    }

    /**
     * When a message is received from the server, by way of {@link Client} and {@link Ludo} this message 
     * gets called to actually add the message to the message area.
     * 
     * @param message information about the message, the user that said it and the content of the message.
     */
	public void newMessage(ChatMessage message) {
		Platform.runLater(()->{
			chatArea.setText(chatArea.getText()+message.getUsername()+": "+message.getMessage()+"\n");
		});
	}
}