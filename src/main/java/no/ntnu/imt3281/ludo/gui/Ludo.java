package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.sun.javafx.collections.ObservableSequentialListWrapper;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.io.dataPackets.ChannelInfo;
import no.ntnu.imt3281.io.dataPackets.ChatMessage;
import no.ntnu.imt3281.io.dataPackets.ChatParticipant;
import no.ntnu.imt3281.io.dataPackets.CreateUser;
import no.ntnu.imt3281.io.dataPackets.JoinChannel;
import no.ntnu.imt3281.io.dataPackets.LeaveChannel;
import no.ntnu.imt3281.io.dataPackets.ListRooms;
import no.ntnu.imt3281.io.dataPackets.LoggedIn;
import no.ntnu.imt3281.io.dataPackets.Login;
import no.ntnu.imt3281.io.dataPackets.MovePiece;
import no.ntnu.imt3281.io.dataPackets.PlayerLeftGame;
import no.ntnu.imt3281.io.dataPackets.RandomGameRequestUpdate;
import no.ntnu.imt3281.io.dataPackets.StartGame;
import no.ntnu.imt3281.io.dataPackets.ThrowDice;
import no.ntnu.imt3281.logging.Logger;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.client.Communication;
import no.ntnu.imt3281.ludo.client.LudoProperties;
import no.ntnu.imt3281.ludo.gui.Ludo.ChannelNameAndParticipants;

/**
 * Main GUI controller, handles the main window with menues and toolbar.
 * Every message received by {@link Client} goes through an object of this class
 * on its way to the correct tab (chat channels and games). 
 * Also, new chat channels and games are created here.
 * 
 * @author oivindk
 *
 */
public class Ludo {
    private static final String I18N_LOCATION = "no.ntnu.imt3281.I18N.i18n";
	@FXML private MenuItem random;
    @FXML private TabPane tabbedPane;
    
    private Communication connection = Communication.getConnection();
	private Window mainWindow;
	private MasterChat masterChat;
	private HashMap<String, Chat> chatrooms = new HashMap<>();
	private HashMap<Integer, GameBoard> games = new HashMap<>();
	private RandomGameDialog randomGameDialog;

	/**
	 * Received from the server when the server want to update the client about
	 * the random game request.
	 * 
	 * @param update contains information about the progress of joining a random game.
	 */
	public void randomGameUpdate(RandomGameRequestUpdate update) {
		Platform.runLater(()->randomGameDialog.update (update));		
	}
	
	/**
	 * Called when user wants to join a random game (through the GUI of the client).
	 * Opens up a dialog ({@link RandomGameDialog}) that informs the user about 
	 * the progress. The user must press the "join random game" button in the dialog to 
	 * actually be put in line for a random game.
	 * 
	 * That is, when no other players are waiting to join a game
	 * nothing happens but as soon as one or more other players also are waiting 
	 * a countdown is started and the number of other players waiting is updated.
	 * The updates are from {@link RandomGameRequestUpdate} objects received from the server.
	 * 
	 * @param e not used
	 */
    @FXML
    public void joinRandomGame(ActionEvent e) {
	    	FXMLLoader loader = new FXMLLoader(getClass().getResource("RandomGameDialog.fxml"));
	    	loader.setResources(ResourceBundle.getBundle(I18N_LOCATION));
	    	VBox pane=null;
		try {
			pane = loader.load();
		} catch (IOException e1) {	// This really shouldn't ever happen
			Logger.getLogger().warning("Unable to load randomGameDialog : "+e.toString());
		}
		randomGameDialog = loader.getController();
	    	Stage dialog = new Stage();
	    	dialog.setTitle(I18N.getString("newuserDialog.title"));
	    	dialog.initOwner(mainWindow);
	    	dialog.initModality(Modality.APPLICATION_MODAL);
	    	dialog.setResizable(false);
	    	Scene scene = new Scene(pane);
	    	dialog.setScene(scene);
	    	randomGameDialog.setStage(dialog);
	    	randomGameDialog.fixI18N();
	    	dialog.showAndWait();
    }
    
    /**
     * Called when "Create new user" in file menu is selected.
     * Uses a {@link CreateUserDialog} to ask the user for details about the new user.
     * If the user fills in the fields and presses OK a {@link CreateUser} object will be sent
     * to the server requesting it to create a new user.
     * If this is successfull a {@link LoggedIn} object will be returned.
     * 
     * @param ae not used
     */
    @FXML
    public void createNewUser(ActionEvent ae) {
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("CreateUserDialog.fxml"));
	   	loader.setResources(ResourceBundle.getBundle(I18N_LOCATION));
		
		AnchorPane pane=null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			Logger.getLogger().warning("Unable to load CreateUserDialog: "+e.getMessage());
		}
		CreateUserDialog controller = loader.getController();
	    	Stage dialog = new Stage();
	    	dialog.setTitle(I18N.getString("newuserDialog.title"));
	    	dialog.initOwner(mainWindow);
	    	dialog.initModality(Modality.APPLICATION_MODAL);
	    	dialog.setResizable(false);
	    	Scene scene = new Scene(pane);
	    	dialog.setScene(scene);
	    	controller.setStage(dialog);
	    	controller.setOKHandler(e->connection.send(controller.getNewUserdata()));
	    	dialog.showAndWait();
    }
    
    /**
     * Called when "Login" in file menu is selected. Use a {@link LoginDialog} to 
     * request username and password from the user. If the user fills in the username
     * and password and pressed OK in the dialog a {@link Login} object will be sent to 
     * server. If the user is logged in the server will reply with a {@link LoggedIn} object.
     * 
     * @param ae not used
     */
    @FXML
    public void login(ActionEvent ae) {
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("LoginDialog.fxml"));
	   	loader.setResources(ResourceBundle.getBundle(I18N_LOCATION));
		
		AnchorPane pane=null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			Logger.getLogger().warning("Unable to load LoginDialog: "+e.getMessage());
		}
		LoginDialog controller = loader.getController();
	    	Stage dialog = new Stage();
	    	dialog.setTitle(I18N.getString("loginDialog.title"));
	    	dialog.initOwner(mainWindow);
	    	dialog.initModality(Modality.APPLICATION_MODAL);
	    	dialog.setResizable(false);
	    	Scene scene = new Scene(pane);
	    	dialog.setScene(scene);
	    	controller.setStage(dialog);
	    	controller.setOKHandler(e->connection.send(controller.getLogindata()));
	    	dialog.showAndWait();
    }
    
    /**
     * Called when user selects menu option or toolbar option to join a chat window.
     * Uses a {@link TextInputDialog} to get the name of the channel to join and then sends
     * message to the server requesting to join that channel.
     * The server will respond with a {@link ChannelInfo} object.
     * 
     * @param ae not used
     */
    @FXML
    public void joinRoom(ActionEvent ae) {
	    	TextInputDialog dialog = new TextInputDialog("");
	    	dialog.setTitle(I18N.getString("joinChannel.title"));
	    	dialog.setHeaderText(I18N.getString("joinChannel.channelName"));
	    	Optional<String> result = dialog.showAndWait();
	    	String name = null;
	    	if (result.isPresent()) {
	    	    name = result.get();
	    	}
	    	if (name!=null) {
	    		connection.send(new JoinChannel(LudoProperties.getProperties().getProperty("token"), name));
	    	}
    }

    /**
     * Called when user selects to list rooms in the menu.
     * Sends message to server to return a list of chat rooms.
     * 
     * @param ae not used
     */
    @FXML
    public void listRooms(ActionEvent ae) {
    		connection.send(new ListRooms(LudoProperties.getProperties().getProperty("token")));
    }
    
    /**
     * Need access to the main window for various reasons :-).
     * 
     * @param mainWindow a reference to the main window of the application
     */
	public void setMainWindow(Window mainWindow) {
		this.mainWindow = mainWindow;
	}

	/**
	 * Used to create the master chat window, will be the first thing to happen when 
	 * communication is established with the server.
	 */
	public void createMasterChat() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("MasterChat.fxml"));
		loader.setResources(ResourceBundle.getBundle(I18N_LOCATION));

		// Use controller to set up communication for this game.
		// Note, a new game tab would be created due to some communication from the server
		// This is here purely to illustrate how a layout is loaded and added to a tab pane.
		
	    	try {
	    		BorderPane masterChat = loader.load();
	        	Tab tab = new Tab("Master chat");
	        	tab.setClosable(false);
	    		tab.setContent(masterChat);
	    		Platform.runLater(()->tabbedPane.getTabs().add(tab));
	    	} catch (IOException e1) {
	    		Logger.getLogger().warning("Error handling master chat tab: "+e1.getMessage());
		}
		masterChat = loader.getController();
	}

	/**
	 * Used to create a chat window. This happens when the client receives a ChannelInfo object
	 * from the server.
	 * 
	 * @param channelInfo an object containing information about the chat channel to create.
	 */
	public void createChat(ChannelInfo channelInfo) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
		loader.setResources(ResourceBundle.getBundle(I18N_LOCATION));

		// Use controller to set up communication for this game.
		// Note, a new game tab would be created due to some communication from the server
		// This is here purely to illustrate how a layout is loaded and added to a tab pane.
		
	    	try {
	    		BorderPane chat = loader.load();
	        	Tab tab = new Tab(channelInfo.getChannelName());
	    		tab.setContent(chat);
	    		tab.setOnClosed(e->{ 		// Send LeaveChannel message when tab is closed
	    			connection.send(
	    					new LeaveChannel(LudoProperties.getProperties().getProperty("token"), 
	    							channelInfo.getChannelName())); 
	    		});
	    		Platform.runLater(()->{
	    			tabbedPane.getTabs().add(tab);
	    			tabbedPane.getSelectionModel().select(tab);
	    		});
	    	} catch (IOException e1) {
			// TODO Auto-generated catch block
	    		Logger.getLogger().warning("Error creating chat tab: "+e1.getMessage());
		}
		Chat chat = loader.getController();
		chat.setChannelInfo(channelInfo);
		chatrooms.put(channelInfo.getChannelName(), chat);
	}

	/**
	 * Called when a message is received.
	 * Finds the right channel and adds the message.
	 * 
	 * @param message the message received for this channel
	 */
	public void chatMessage(ChatMessage message) {
		if (message.getChannel()==null) {
			Platform.runLater(()->masterChat.newMessage(message));
		} else {
			Platform.runLater(()->chatrooms.get(message.getChannel()).newMessage(message));
		}
	}

	/**
	 * Called when a ChatParticipant object is received from the server. 
	 * Finds the right channel and sends the message to that channel.
	 * The channel object will then add or remove the user to/from the user list.
	 * 
	 * @param cp information about the channel and participant.
	 */
	public void enterLeaveChannel(ChatParticipant cp) {
		Platform.runLater(()->chatrooms.get(cp.getChannelName()).enterLeaveChannel(cp));
	}

	/**
	 * Called when an object is received from the server telling the client
	 * to start up a new game.
	 * Creates a new tab and initiates a game in this tab.
	 * 
	 * @param gameInfo contains the game id and the names of the players in the game.
	 */
	public void startNewGame(StartGame gameInfo) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
		loader.setResources(ResourceBundle.getBundle(I18N_LOCATION));

		// Use controller to set up communication for this game.
		// Note, a new game tab would be created due to some communication from the server
		// This is here purely to illustrate how a layout is loaded and added to a tab pane.
		
	    	try {
	    		ScrollPane gameBoard = loader.load();
	        	Tab tab = new Tab(gameInfo.getName());
	    		tab.setContent(gameBoard);
	    		tab.setOnClosed(e->{ 		// Send LeaveChannel message when tab is closed
	    			connection.send(
	    					new PlayerLeftGame(gameInfo.getGameCode(), 
	    							LudoProperties.getProperties().getProperty("token"))); 
	    		});
	        	Platform.runLater(()-> {
	        		tabbedPane.getTabs().add(tab);
	        		tabbedPane.getSelectionModel().select(tab);
	        	});
	    	} catch (IOException e1) {
	    		Logger.getLogger().warning("Unable to create game tab: "+e1.getMessage());
		}
		GameBoard ludoBoard = loader.getController();
		Platform.runLater(()->ludoBoard.initializeGame(gameInfo));
		games.put(gameInfo.getGameCode(), ludoBoard);
	}

	/**
	 * Called when an object is received from the server informing about a
	 * dice being thrown in a game.
	 * Finds the right game object and informs about the dice thrown.
	 * 
	 * @param diceThrown contains information about the game id and the dice value.
	 */
	public void diceThrown(ThrowDice diceThrown) {
		games.get(diceThrown.getGameId()).throwDice(diceThrown.getValue());
	}

	/**
	 * Called when an object is received from the server telling this client to move
	 * a piece.
	 * Finds the right game and performs the move.
	 * 
	 * @param movePiece contains information about what game, piece and movement to perform.
	 */
	public void movePiece(MovePiece movePiece) {
		games.get(movePiece.getGameId()).movePiece(movePiece.getPlayer(), movePiece.getFrom(), movePiece.getTo());
		games.get(movePiece.getGameId()).updateBoard();
	}

	/**
	 * Called when a PlayerLeftGame is received from the server
	 * Finds the right game and removes the player from the game.
	 * 
	 * @param playerGameInfo contains the id of the game and the name of the player.
	 */
	public void playerLeftGame(PlayerLeftGame playerGameInfo) {
		games.get(playerGameInfo.getGameId()).removePlayer(playerGameInfo.getUsername());
	}

	/**
	 * Called when ListRooms are received from server.
	 * Opens a dialog showing the list of rooms i a listview. The user can open a room/channel
	 * by double clicking on the room name.
	 * 
	 * @param listOfRooms contains a map of room names and number of participants.
	 */
	public void roomListReceived(ListRooms listOfRooms) {
		// https://examples.javacodegeeks.com/desktop-java/javafx/dialog-javafx/javafx-dialog-example/
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle(I18N.getString("roomListDialog.title"));
		dialog.setHeaderText(I18N.getString("roomListDialog.headerText"));
		dialog.setResizable(true);
		ListView<ChannelNameAndParticipants> channels = new ListView<>();
		ObservableList<ChannelNameAndParticipants> items =FXCollections.observableArrayList (new ChannelNameAndParticipants("" ,1));
		items.clear();
		// Take information from the map and transfer to a ObservableList with ChannelNameAndParticipants
		listOfRooms.getRoomParticipants().entrySet().forEach(e->{
			items.add(new ChannelNameAndParticipants(e.getKey(), e.getValue()));
		});
		channels.setItems(items);
		channels.setCellFactory(channelView -> new ChannelViewCell());
		channels.setOnMouseClicked(e-> {
			if (e.getClickCount() == 2) {
				ChannelNameAndParticipants c = channels.getSelectionModel().getSelectedItem();
				connection.send(new JoinChannel(LudoProperties.getProperties().getProperty("token"), c.getName()));
			}
		});
		dialog.getDialogPane().setContent(channels);
		
		ButtonType buttonTypeCancel = new ButtonType(I18N.getString("button.cancel"), ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeCancel);

		dialog.showAndWait();
	}
	
	/**
	 * Used for "painting" the cells in the listview in {@link Ludo#roomListReceived(ListRooms)}.
	 * 
	 * @author oivindk
	 *
	 */
	class ChannelViewCell extends ListCell<ChannelNameAndParticipants> {
		/**
		 * @see javafx.scene.control.Cell#updateItem(T, boolean)
		 */
		protected void updateItem(ChannelNameAndParticipants channel, boolean empty) {
	        super.updateItem(channel, empty);
	        if (empty||channel==null) {
		        	setText(null);
		        	setGraphic(null);
	        } else {
		        BorderPane pane = new BorderPane();
		        Label name = new Label(channel.getName());
		        name.setAlignment(Pos.CENTER_LEFT);
		        pane.setLeft(new Label(channel.getName()));
		        pane.setRight(new Label("("+Integer.toString(channel.getParticipants())+")"));
		        setText(null);
		        setGraphic(pane);
	        }
		}
	}
	
	/**
	 * Holds information about a chat channels name and number of participants.
	 * Used by the listview in the dialog showing channel names and number of participants.
	 * 
	 * @author oivindk
	 *
	 */
	class ChannelNameAndParticipants {
		private String name; 
		private int participants;
		
		/**
		 * Create a new object with a name and a number of participants.
		 * 
		 * @param name the name of the channel
		 * @param participants the number of participants in the channel
		 */
		public ChannelNameAndParticipants (String name, int participants) {
			this.name = name;
			this.participants = participants;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the participants
		 */
		public int getParticipants() {
			return participants;
		}
	}
}
