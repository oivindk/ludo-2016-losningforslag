package no.ntnu.imt3281.ludo.server;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import javax.swing.JOptionPane;

import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.io.Tools;
import no.ntnu.imt3281.io.dataPackets.ChannelInfo;
import no.ntnu.imt3281.io.dataPackets.ChatMessage;
import no.ntnu.imt3281.io.dataPackets.ChatParticipant;
import no.ntnu.imt3281.io.dataPackets.CreateUser;
import no.ntnu.imt3281.io.dataPackets.GameChallenge;
import no.ntnu.imt3281.io.dataPackets.JoinChannel;
import no.ntnu.imt3281.io.dataPackets.JoinRandomGame;
import no.ntnu.imt3281.io.dataPackets.LeaveChannel;
import no.ntnu.imt3281.io.dataPackets.ListRooms;
import no.ntnu.imt3281.io.dataPackets.LoggedIn;
import no.ntnu.imt3281.io.dataPackets.Login;
import no.ntnu.imt3281.io.dataPackets.MovePiece;
import no.ntnu.imt3281.io.dataPackets.PlayerLeftGame;
import no.ntnu.imt3281.io.dataPackets.Pong;
import no.ntnu.imt3281.io.dataPackets.RandomGameRequestUpdate;
import no.ntnu.imt3281.io.dataPackets.ThrowDice;
import no.ntnu.imt3281.logging.Logger;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * 
 * This is the main class for the server. 
 * **Note, change this to extend other classes if desired.**
 * 
 * @author Øivind Kolloen
 *
 */
public class Server {
	private static final int TIME_TO_WAIT_FOR_RANDOM_PLAYERS = 5;		// TODO: Change to 30 for deployment
	static Database database = Database.getDatabase();
	DatagramSocket serverSocket;
	private Properties properties = new Properties();
	private HashMap<String, ClientConnection> clients = new HashMap<>();
	private HashMap<String, ArrayList<ClientConnection>> chats = new HashMap<>();
	private HashMap<Integer, Game> games = new HashMap<>();
	private HashMap<String, ClientConnection> randomGameRequests = new HashMap<>();
	private int waitForRandomPlayersTimer = 30;
	private boolean stopping = false;
	private java.util.logging.Logger logger = java.util.logging.Logger.getLogger(getClass().getName());
	
	public Server () {
		InputStream is = getClass().getResourceAsStream("/ludo.config");
		if (is!=null) {
			try {
				properties.load(is);
				is.close();
			} catch (IOException e) {
				logger.log(Level.INFO, "Unable to load config file", e);
			}
		}
		try {
			serverSocket = new DatagramSocket(Integer.parseInt(properties.getProperty("serverPort", "6789")));
		} catch (NumberFormatException e1) {
			logger.log(Level.SEVERE, "Unable to open server socket on port", e1);
		} catch (SocketException e1) {
			logger.log(Level.SEVERE, "Unable to open server socket", e1);
			System.exit(-1);
		}
		ExecutorService pool = Executors.newCachedThreadPool();
		Pinger pinger = new Pinger(clients, serverSocket, e->
			removeLostUser(e)
		);
		pool.execute(pinger);
		pool.execute(()->waitForRandomPlayers());
		pool.execute(()-> {
			try {
				byte[] buffer = new byte[1024*16];
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				while (!stopping) {
					Thread.sleep(10);
					serverSocket.receive(packet);
					Object o = Tools.convertFromBytes(packet.getData());
					handleClientMessage(packet, o); 
				}
			} catch (Exception e) {
				logger.log(Level.INFO, "Communication error", e);
			}
		});
	}

	/**
	 * All message from the clients are handled in this method.
	 * 
	 * @param packet a datagram packet with a 16KB buffer user to send messages to the clients
	 * @param object the object received form a client, from no.ntnu.imt3281.io.dataPackets
	 * @throws Exception
	 */
	private void handleClientMessage(DatagramPacket packet, Object object) throws DuplicateUserException {
		if (object instanceof CreateUser) {					// Create user
			createUser(packet, object);
		} else if (object instanceof Login) {				// Login
			loginUser(packet, object);
		} else if (object instanceof JoinChannel) {			// Join channel
			joinChannel((JoinChannel)object);
		} else if (object instanceof ChatMessage) {			// Chat message
			ChatMessage message = (ChatMessage)object;
			if (message.getChannel()==null) {				// Master chat
				MessageLogger.log(clients.get(message.getToken()).getId(), message.getMessage());
				broadcast (clients, new ChatMessage(message.getMessage(), null, clients.get(message.getToken()).getUsername()));
			} else if (chats.containsKey(message.getChannel())) {	// Chat channel
				MessageLogger.log(clients.get(message.getToken()).getId(), message.getChannel(), message.getMessage());
				broadcast(chats.get(message.getChannel()), new ChatMessage(message.getMessage(), null, clients.get(message.getToken()).getUsername(), message.getChannel()));
			}
		} else if (object instanceof LeaveChannel) {			// Leave channel
			leaveChannel((LeaveChannel)object);
		} else if (object instanceof JoinRandomGame) {		// Join random game
			joinRandomGame(((JoinRandomGame)object).getToken());
		} else if (object instanceof GameChallenge) {		// Challenge players to game
			gameChallenge((GameChallenge)object);
		} else if (object instanceof ThrowDice) {			// Throw a dice in given game
			games.get(((ThrowDice)object).getGameId()).throwDice();
		} else if (object instanceof MovePiece) { 
			MovePiece mp = (MovePiece)object;
			games.get(mp.getGameId()).movePiece(mp.getPlayer(), mp.getFrom(), mp.getTo());
		} else if (object instanceof PlayerLeftGame) { 
			playerLeftGame((PlayerLeftGame)object);
		} else if (object instanceof ListRooms) { 
			listRooms((ListRooms)object);
		} else if (object instanceof Pong) {					// Ping reply
			Pong pong = (Pong)object;
			clients.get(pong.getToken()).setPingReply(true);
		}
	}

	/**
	 * Called when a ListRooms object is received from a client.
	 * Sends back a list of rooms if the list of rooms is less than 64KB.
	 * TODO: Figure out what to do if the list of rooms exceed 64KB.
	 * 
	 * @param rooms contains the token for the user requesting the list.
	 */
	private void listRooms(ListRooms rooms) {
		HashMap<String, Integer> roomParticipants = new HashMap<>();
		chats.entrySet().forEach(e->{
			roomParticipants.put(e.getKey(), e.getValue().size());
		});
		rooms.setRooms(roomParticipants);
		try {
			byte[] buffer = Tools.convertToBytes(rooms);
			if (buffer.length<1024*64)
				send(rooms, clients.get(rooms.getToken()).getSocketAddress());
		} catch (IOException e) {
			logger.log(Level.INFO, "Unable to send room list", e);
		}		
	}

	/**
	 * Removes player with given token from the game. Also removes the game if it is empty.
	 * 
	 * @param gameInfo contains game id and user token.
	 */
	private void playerLeftGame(PlayerLeftGame gameInfo) {
		Game game = games.get(gameInfo.getGameId());
		game.removePlayer(clients.get(gameInfo.getToken()).getUsername());
		game.getPlayers().remove(clients.get(gameInfo.getToken()));
		if (game.getPlayers().isEmpty()) {
			synchronized (games) {
				games.remove(gameInfo.getGameId());
			}
		} else {
			broadcast(game.getPlayers(), new PlayerLeftGame(gameInfo.getGameId(), clients.get(gameInfo.getToken()).getUsername()));
		}
	}

	private void gameChallenge(GameChallenge challenge) {
		String challengingPlayer = clients.get(challenge.getToken()).getUsername();
		Game game = new Game("Challenge: "+challengingPlayer);
		game.addPlayer(challenge.getToken(), clients.get(challenge.getToken()));
		synchronized (clients) {
			clients.entrySet().parallelStream().forEach(e->{
				for (String uname : challenge.getPlayers()) {
					if (e.getValue().getUsername().equals(uname)&&!e.getValue().getUsername().equals(challengingPlayer)) {
						game.addPlayer(e.getKey(), e.getValue());
					}
				}
			});
		}
		if (game.getPlayers().size()>1) {	// At least two players
			game.addDiceListener(de->
				broadcast(((Game)de.getSource()).getPlayers(), new ThrowDice(game.hashCode(), de.getDice()))
			);
			game.addPieceListener(pe->
				broadcast(((Game)pe.getSource()).getPlayers(), new MovePiece(game.hashCode(), pe.getPlayer(), pe.getFrom(), pe.getTo()))
			);
			games.put(new Integer(game.hashCode()), game);
			broadcast(game.getPlayers(), game.getStartGame());
		}
	}

	/**
	 * Called from the ping thread when there is missing ping reply
	 * 
	 * @param ae the token for the user can be found by getActionCommand()
	 */
	private void removeLostUser(ActionEvent ae) {
		// e.getActionCommand contains token for user who has left
		String token = ae.getActionCommand();
		removeUserFromChatRooms(token);
		removeUserFromGames(token);
	}

	private void removeUserFromGames(String token) {
		games.entrySet().parallelStream().forEach(e->{	// Go trough all games and remove player
			if (e.getValue().getPlayers().contains(clients.get(token))) {
				broadcast(e.getValue().getPlayers(), new PlayerLeftGame(e.getKey(), clients.get(token).getUsername()));
				e.getValue().removePlayer(clients.get(token).getUsername());
				synchronized (e.getValue().getPlayers()) {
					e.getValue().getPlayers().remove(clients.get(token));					
				}
			}
		});
		synchronized (games) {	// Go trough all games and remove empty ones
			Iterator<Entry<Integer, Game>> iterator = games.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<Integer, Game> e = iterator.next();
				if (e.getValue().getPlayers().isEmpty())
					iterator.remove();
			}
		}
	}

	private void removeUserFromChatRooms(String token) {
		chats.entrySet().parallelStream().forEach(e->{	// Remove from user from chat rooms
			if (e.getValue().contains(clients.get(token))) {
				broadcast(e.getValue(), new ChatParticipant(e.getKey(), clients.get(token).getUsername(), ChatParticipant.LEAVE));
				synchronized (e.getValue()) {
					e.getValue().remove(clients.get(token));
				}
			}
		});
		synchronized (chats) {
			// Go trough chat rooms and remove any that are empty
			Iterator<Entry<String, ArrayList<ClientConnection>>> iterator = chats.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, ArrayList<ClientConnection>> e = iterator.next();
				if (e.getValue().isEmpty()) {
					iterator.remove();
				}
			}			
		}
	}

	private void waitForRandomPlayers() {
		while (!stopping) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Logger.getLogger().fine("Sleep interupted in waitForRandomPlayers");
				Thread.currentThread().interrupt();
			}
			waitForRandomPlayersTimer--;
			synchronized (randomGameRequests) {	// Only one thread at a time can manipulate this map
				if (randomGameRequests.size()>1) {
					if (waitForRandomPlayersTimer<0) {
						startRandomGame();
					} else {
						broadcast(randomGameRequests, new RandomGameRequestUpdate(waitForRandomPlayersTimer, randomGameRequests.size()));
					}
				}
			}
			if (waitForRandomPlayersTimer<0&&randomGameRequests.size()>1) {
				startRandomGame();
			}
		}
	}

	/**
	 * Called in response to receiving a JoinRandomGame object.
	 * 
	 * @param token the token for the user asking to join the game.
	 */
	private void joinRandomGame(String token) {
		waitForRandomPlayersTimer = TIME_TO_WAIT_FOR_RANDOM_PLAYERS;
		synchronized (randomGameRequests) {		// Only one thread at a time can manipulate this map
			randomGameRequests.put(token, clients.get(token));
			if (randomGameRequests.size()==4) {
				broadcast(randomGameRequests, new RandomGameRequestUpdate(0, 4));	// This hides the JoinRandomGame dialog on clients.
				startRandomGame();
			}
		}
	}

	/**
	 * Called when we have four players waiting for a random game OR
	 * two or more players have been waiting for 30 seconds.
	 */
	private void startRandomGame() {
		synchronized (randomGameRequests) {		// Only one thread at a time can manipulate this map 
			Game game = new Game("Random Game");
			randomGameRequests.entrySet().parallelStream().forEach(e->game.addPlayer(e.getKey(), e.getValue()));
			game.addDiceListener(de->
				broadcast(((Game)de.getSource()).getPlayers(), new ThrowDice(game.hashCode(), de.getDice()))
			);
			game.addPieceListener(pe->
				broadcast(((Game)pe.getSource()).getPlayers(), new MovePiece(game.hashCode(), pe.getPlayer(), pe.getFrom(), pe.getTo()))
			);
			games.put(new Integer(game.hashCode()), game);
			broadcast(randomGameRequests, game.getStartGame());
			randomGameRequests.clear();
		}
	}

	/**
	 * Called in response to receiving a LeaveChannel object. 
	 * Might also be called when a user no longer exists (removed by missing pong.)
	 * 
	 * @param lc information about the channel and user that is leaving
	 */
	private void leaveChannel(LeaveChannel lc) {
		String uname = clients.get(lc.getToken()).getUsername();
		chats.get(lc.getChannelName()).remove(clients.get(lc.getToken()));
		if (!chats.get(lc.getChannelName()).isEmpty()) {
			broadcast(chats.get(lc.getChannelName()), new ChatParticipant(lc.getChannelName(), uname, ChatParticipant.LEAVE));
		} else {
			chats.remove(lc.getChannelName());
		}
	}

	/**
	 * Called in response to receiving a JoinChannel object.
	 * Register the user on the channel, send username to all participants on the chat and the userlist to 
	 * the client just registered on the chat.
	 * 
	 * @param object containing the name of the channel to join and the token of the user that 
	 * requested to join the channel.
	 */
	private void joinChannel(JoinChannel joinRequest) {
		ArrayList<ClientConnection> channel;
		if (chats.containsKey(joinRequest.getChannelName())) {
			String userName = clients.get(joinRequest.getToken()).getUsername();
			channel = chats.get(joinRequest.getChannelName());
			broadcast(channel, new ChatParticipant(joinRequest.getChannelName(), userName, ChatParticipant.JOIN));
			channel.add(clients.get(joinRequest.getToken()));
		} else {
			ArrayList<ClientConnection> chat = new ArrayList<>();
			chat.add(clients.get(joinRequest.getToken()));
			chats.put(joinRequest.getChannelName(), chat);
			channel = chat;
		}
		send(new ChannelInfo(joinRequest.getChannelName(), channel), clients.get(joinRequest.getToken()).getSocketAddress());
	}

	/**
	 * Method called from main message loop to log a user in to the system.
	 * 
	 * @param packet the received data packet
	 * @param o an object with information about the user to log in (username, password)
	 */
	private void loginUser(DatagramPacket packet, Object o) {
		Login login = (Login)o;
		String token = database.login(login.getUsername(), login.getPassword());
		if (token!=null) {	// If a token was returned the login was successful.
			ClientConnection client = new ClientConnection(packet.getSocketAddress());
			int id = database.validateToken(token);
			if (id>-1) {
				client.setId(id);
				String info[] = database.getUserInfo(id);
				client.setGivenname(info[1]);
				client.setSurename(info[2]);
			}
			client.setUsername(login.getUsername());
			synchronized (clients) {
				clients.put(token, client);
			}
			send (new LoggedIn(token), packet.getSocketAddress());
		}
	}

	/**
	 * Method called from main message loop to create a new user.
	 * 
	 * @param packet the received data packet
	 * @param o the object with information about the new user
	 * @throws Exception 
	 */
	private void createUser(DatagramPacket packet, Object o) throws DuplicateUserException {
		CreateUser newUser = (CreateUser)o;
		String token = database.createUser(newUser.getUsername(), newUser.getPassword(), 
				newUser.getGivenname(), newUser.getSurename(), newUser.getEmail());
		if (token!=null) {	// If token returned a new user was created
			ClientConnection client = new ClientConnection(packet.getSocketAddress());
			client.setUsername(newUser.getUsername());
			client.setGivenname(newUser.getGivenname());
			client.setSurename(newUser.getSurename());
			synchronized (clients) {
				clients.put(token, client);
			}
			send (new LoggedIn(token), packet.getSocketAddress());	// Automatically logged in
		}
	}

	/**
	 * Send given object to all clients in the given map
	 * 
	 * @param clients a map containing the clients.
	 * @param object the object to send.
	 */
	private void broadcast(HashMap<String, ClientConnection> clients, Object object) {
		byte buffer[];
		try {
			buffer = Tools.convertToBytes(object);
			clients.entrySet().parallelStream().forEach(e->{
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length, e.getValue().getSocketAddress());
				try {
					serverSocket.send(packet);
				} catch (IOException e1) {
					logger.log(Level.INFO, "Error sending data to client", e1);
				}
			});
		} catch (IOException e1) {
			logger.log(Level.INFO, "Error converting object to bytes", e1);
		}
	}

	/**
	 * Send given object to all clients in the given arraylist
	 * 
	 * @param arrayList a collection containing the clients to send the object to
	 * @param object the object to send
	 */
	private void broadcast(ArrayList<ClientConnection> clients, Object object) {
		byte buffer[];
		try {
			buffer = Tools.convertToBytes(object);
			clients.parallelStream().forEach(e->{
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length, e.getSocketAddress());
				try {
					serverSocket.send(packet);
				} catch (IOException e1) {
					logger.log(Level.INFO, "Error sending data to client", e1);
				}
			});
		} catch (IOException e1) {
			logger.log(Level.INFO, "Error converting object to bytes", e1);
		}
	}
	
	/**
	 * Send an object to the given address.
	 * 
	 * @param object the object to send.
	 * @param address the address to send the object to.
	 * @return true if object sent without error, false otherwise.
	 */
	public boolean send (Object object, SocketAddress address) {
		byte buffer[];
		try {
			buffer = Tools.convertToBytes(object);
    		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address);
    		serverSocket.send(packet);
		} catch (IOException e1) {
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(I18N.getString("Starting"));
		Server server = new Server();
		server.stopping = false;
		JOptionPane.showConfirmDialog(null, "Close dialog to close server");
		MessageLogger.close();
		System.exit(0);
	}
}
