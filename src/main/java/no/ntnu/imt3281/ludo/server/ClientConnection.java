package no.ntnu.imt3281.ludo.server;

import java.net.SocketAddress;

public class ClientConnection {
	private SocketAddress address;
	private boolean pingReply=true;
	private String username;
	private String givenname;
	private String surename;
	private int id;
	
	public ClientConnection(SocketAddress address) {
		this.address = address;
	}

	/**
	 * @return the address
	 */
	public SocketAddress getSocketAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setSocketAddress(SocketAddress address) {
		this.address = address;
	}

	/**
	 * @return the pingReply
	 */
	public boolean isPingReply() {
		return pingReply;
	}

	/**
	 * @param pingReply the pingReply to set
	 */
	public void setPingReply(boolean pingReply) {
		this.pingReply = pingReply;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the givenname
	 */
	public String getGivenname() {
		return givenname;
	}

	/**
	 * @param givenname the givenname to set
	 */
	public void setGivenname(String givenname) {
		this.givenname = givenname;
	}

	/**
	 * @return the surename
	 */
	public String getSurename() {
		return surename;
	}

	/**
	 * @param surename the surename to set
	 */
	public void setSurename(String surename) {
		this.surename = surename;
	}

	/**
	 * Set id of this user.
	 * 
	 * @param id the id of the user.
	 */
	public void setId(int id) {
		this.id=id;
	}
	
	/**
	 * Get the id of this user.
	 * 
	 * @return the id of the user
	 */
	public int getId() {
		return id;
	}
}
