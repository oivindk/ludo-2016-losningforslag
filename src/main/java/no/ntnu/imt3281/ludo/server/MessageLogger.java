package no.ntnu.imt3281.ludo.server;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.I18N.I18N;

/**
 * Singleton object of this class is used to log chat messages to file.
 * 
 * @author oivindk
 *
 */
public class MessageLogger {
	private static MessageLogger logger1 = new MessageLogger();
	private static BufferedWriter bw;
	private static Logger logger;
	
	private MessageLogger () {
		try {
			FileWriter fw = new FileWriter("./messageLog.log", true);
			bw = new BufferedWriter(fw);
		} catch (IOException e) {
			logger.log(Level.INFO, "Unable to open log file", e);
		}
		MessageLogger.logger = Logger.getLogger(getClass().getName());;
	}
	
	/**
	 * Logs a global chat message to file. The current date/time will be added to the logged record.
	 * 
	 * @param userId the id of the user sending the message.
	 * @param message the message to log.
	 */
	public static void log (int userId, String message) {
		Object[] data = new Object[3];
		data[0] = new Date();
		data[1] = userId;
		data[2] = message;
		try {
			synchronized (bw) {
				bw.write(I18N.getConcatenatedString("log.globalChat", data));
				bw.newLine();				
			}
		} catch (IOException e) {
			logger.log(Level.INFO, "Unable to write log record", e);
		}
	}
	
	/**
	 * Logs a global chat channel message to file. The current date/time will be added to the logged record.
	 *
	 * @param userId the token of the user sending the message.
	 * @param channel the name of the channel the message was sent in
	 * @param message the message to log.
	 */
	public static void log (int userId, String channel, String message) {
		Object[] data = new Object[4];
		data[0] = new Date();
		data[1] = userId;
		data[2] = message;
		data[3] = channel;
		try {
			synchronized (bw) {
				bw.write(I18N.getConcatenatedString("log.chatChannel", data));
				bw.newLine();
			}
		} catch (IOException e) {
			logger.log(Level.INFO, "Unable to write log record", e);
		}
	}

	/**
	 * Logs a global game chat message to file. The current date/time will be added to the logged record.
	 *
	 * @param userId the token of the user sending the message.
	 * @param game the id of the game the message was sent in
	 * @param message the message to log.
	 */
	public static void log (int userId, int game, String message) {
		Object[] data = new Object[4];
		data[0] = new Date();
		data[1] = userId;
		data[2] = message;
		data[3] = game;
		try {
			synchronized (bw) {
				bw.write(I18N.getConcatenatedString("log.gameMessage", data));
				bw.newLine();
			}
		} catch (IOException e) {
			logger.log(Level.INFO, "Unable to write log record", e);
		}
	}
	
	/**
	 * Closes the log file. Should be called before the game exits.
	 */
	public static void close() {
		try {
			synchronized (bw) {
				bw.close();
			}
		} catch (IOException e) {
			logger.log(Level.INFO, "Unable to open log file", e);
		}
	}
}
