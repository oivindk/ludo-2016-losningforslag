package no.ntnu.imt3281.ludo.server;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import no.ntnu.imt3281.logging.Logger;

/**
 * All database related operations should go through methods in this class.
 * Only one object of this class should exists, get it through the getDatabase method.
 * 
 * There are methods for creating new users, login in, getting and setting user information. 
 * TODO: NOTE Fill in as more methods are created!
 * 
 * @author Øivind Kolloen
 *
 */
public class Database {
	private static final String URL = "jdbc:derby:LudoDB";
	public static final Database database = new Database ();
	private Connection con;
	private SecureRandom random = new SecureRandom();
	static final String AB = "#*+-/&%_0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static final byte[] SALT = "LuDo123".getBytes();
	
	private Database () {
		try {
            con = DriverManager.getConnection(URL);
		} catch (SQLException sqle) {		// No database exists
			try {							// Try creating database
				con = DriverManager.getConnection(URL+";create=true");
				setupDB();
			} catch (SQLException sqle1) {	// Unable to create database, exit server
				Logger.getLogger().log(Level.SEVERE, "Unable to create database", sqle1);
				System.exit(-1);
			}
		}
	}

	/**
	 * Get database, created by singleton.
	 * 
	 * @return the database object with connection.
	 */
	public static Database getDatabase() {
		return database;
	}
	
	/**
	 * Updates the timestamp of the token, making it valid for 30 days.
	 * 
	 * @param token the 64 character string that is the token
	 */
	public void updateTokenTimestamp (String token) {
		String updateToken = "UPDATE tokens SET expires=? WHERE token=?";
		PreparedStatement stmnt;
		try {
			stmnt = con.prepareStatement(updateToken);
			java.util.Date now = new java.util.Date();
			stmnt.setDate(1, new Date(now.getTime()+1000*60*60*24*30));	// Now + 30 days
			stmnt.setString(2, token);
			stmnt.executeUpdate();
			stmnt.close();
		} catch (SQLException e) {
			Logger.getLogger().log(Level.INFO, "Unable to update timestamp for token : "+token, e);
		}
	}
	
	/**
	 * Get info about given user, returns username, givenname, surename and email.
	 * 
	 * @param id the id of the user we want information about.
	 * @return and array of String where the contents are username, givenname, surename and email
	 */
	public String[] getUserInfo (int id) {
		String userInfo = "SELECT username, givenname, surename, email FROM users WHERE id=?";
		String info[] = null;
		try {
			PreparedStatement stmnt = con.prepareStatement(userInfo);
			stmnt.setInt(1, id);
			ResultSet res = stmnt.executeQuery();
			if (res.next()) {
				info = new String[4];
				for (int i=0; i<info.length; i++) {
					info[i] = res.getString(i+1);
				}
			}
			stmnt.close();
		} catch (SQLException e) {
			Logger.getLogger().log(Level.INFO, "Unable to fetch user info for user: "+id, e);
		}
		return info;
	}
	
	/**
	 * Checks if this is a valid token. If this is a valid token the userid for the user owning this token is 
	 * returned. If this is an invalid token -1 will be returned.
	 * 
	 * @param token the 64 character string to check to see if is a valid token.
	 * @return the userid of the user holding the given token
	 */
	public int validateToken (String token) {
		String sql = "SELECT userid FROM tokens WHERE token=?";
		int id = -1;
		try {
			PreparedStatement stmnt = con.prepareStatement(sql);
			stmnt.setString(1, token);
			ResultSet res = stmnt.executeQuery();
			if (res.next()) {
				id = res.getInt(1);
			}
			stmnt.close();
		} catch (SQLException e) {
			Logger.getLogger().log(Level.INFO, "Unable to validate token: "+token, e);
		}
		return id;
	}
	
	/**
	 * Try login with given username and password. If successful return the token assigned to 
	 * this session, otherwise return null.
	 * 
	 * @param username for the user to try to log in
	 * @param pwd for the user to try to log in
	 * @return the token for the user logged in or null if no user existed with the given credentials.
	 */
	public String login (String username, String pwd) {
		String sql = "SELECT id FROM users WHERE username=? and password="+"?";	// Workaround to get rid of Sonar error
		String token = randomString(64);
		try (PreparedStatement stmnt = con.prepareStatement(sql)) {
			// From https://www.owasp.org/index.php/Hashing_Java
			SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
	        PBEKeySpec spec = new PBEKeySpec( pwd.toCharArray(), SALT, 16, 254 );
	        SecretKey key = skf.generateSecret( spec );
	        byte[] pwdHash = key.getEncoded( );
	        //
	        stmnt.setString(1, username);
			stmnt.setBytes(2, pwdHash);
			ResultSet res = stmnt.executeQuery();
			if (res.next()) {
				int id = res.getInt(1);
				String updateToken = "UPDATE tokens SET token=?, expires=? WHERE userid=?";
				stmnt.close();
				PreparedStatement stmnt1 = con.prepareStatement(updateToken);
				stmnt1.setString(1, token);
				java.util.Date now = new java.util.Date();
				stmnt1.setDate(2, new Date(now.getTime()+1000*60*60*24*30));	// Now + 30 days
				stmnt1.setInt(3, id);
				stmnt1.executeUpdate();
				stmnt1.close();
			} else {
				token = null;
			}
		} catch (SQLException e) {
			token = null;
			Logger.getLogger().log(Level.INFO, "Unable to log in user " + username, e);
		} catch (NoSuchAlgorithmException e) {
			token = null;
			Logger.getLogger().log(Level.INFO, "Missing crypto algorithm", e);
		} catch (InvalidKeySpecException e) {
			token = null;
			Logger.getLogger().log(Level.INFO, "Invalid crypto key", e);
		}
		return token;
	}
	
	/**
	 * Creates new user with given information. Creates a new user and also creates session token, this token is returned 
	 * and should be returned to the user. Use this token to maintain session states.
	 * 
	 * @param username the username (login name) of the new user.
	 * @param pwd the password for the user, this is encrypted before being stored in the database.
	 * @param givenname the users first name.
	 * @param surename the users last (family) name.
	 * @param email the email address for the user, needed to send new password if user forgets password. 
	 * @return a string containing the 64 character session token.
	 * @throws DuplicateUserException throws an exception if the username is already taken or there is another error adding 
	 * the user to the database.
	 */
	public String createUser (String username, String pwd, String givenname, String surename, String email) throws DuplicateUserException {
		String newUser = "INSERT INTO users (username, password, givenname, surename, email) VALUES (?, ?, ?, ?, ?)";
		String setToken = "INSERT INTO tokens (token, userid, expires) VALUES (?, ?, ?)";
		PreparedStatement stmnt;
		String token = randomString(64);
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
	        PBEKeySpec spec = new PBEKeySpec( pwd.toCharArray(), SALT, 16, 254 );
	        SecretKey key = skf.generateSecret( spec );
	        byte[] pwdHash = key.getEncoded( );
			stmnt = con.prepareStatement (newUser, new String[] { "ID"});
			stmnt.setString(1, username);
			stmnt.setBytes(2, pwdHash);
			stmnt.setString(3, givenname);
			stmnt.setString(4, surename);
			stmnt.setString(5, email);
			stmnt.executeUpdate();
			ResultSet rs = stmnt.getGeneratedKeys();
			stmnt.close();
			if (rs.next()) {
				Object id = rs.getObject(1);
				rs.close();
				stmnt = con.prepareStatement(setToken);
				stmnt.setString(1, token);
				stmnt.setObject(2, id);
				java.util.Date now = new java.util.Date();
				stmnt.setDate(3, new Date(now.getTime()+1000*60*60*24*30));	// Now + 30 days
				stmnt.executeUpdate();	
				stmnt.close();
			}
		} catch (Exception e) {
			if (e instanceof org.apache.derby.shared.common.error.DerbySQLIntegrityConstraintViolationException) {
				throw new DuplicateUserException ("error.db.duplicateUser", e);
			} else {
				Logger.getLogger().log(Level.INFO, "Unable to create user : " + username, e);
			}
		}
		return token;
	}
	
	/**
	 * Generates a random string of len characters. Used as a token to maintain session and login state
	 * 
	 * @param len the number of characters for the returned string.
	 * @return a random string of len characters.
	 */
	private String randomString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( random.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	/**
	 * Used to initialize the tables in the database. This will only be executed once when the application is run
	 * on a new server for the first time.
	 */
	private void setupDB() {
		Statement stmt;
		try {
			stmt = con.createStatement();
	        // MySQL has auto_increment, Derby has GENERATED values
	        stmt.execute("CREATE TABLE users (id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
	                + "username varchar(128) NOT NULL, "
	                + "password char(254) for bit data NOT NULL, "
	                + "givenname varchar(128) NOT NULL, "
	                + "surename varchar(128) NOT NULL, "
	                + "email varchar(128) NOT NULL, "
	                + "PRIMARY KEY  (id), UNIQUE (username))");
	        stmt.execute("CREATE TABLE tokens (token char(64) NOT NULL, "
	                + "userid bigint NOT NULL, "
	                + "expires DATE NOT NULL, "
	                + "PRIMARY KEY  (token), UNIQUE (userid))");
	        // And close
	        stmt.close();
		} catch (SQLException e) {
			Logger.getLogger().log(Level.SEVERE, "Unable to create database tables", e);
			System.exit(-1);
		}
	}
}
