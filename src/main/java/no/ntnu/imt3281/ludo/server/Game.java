package no.ntnu.imt3281.ludo.server;

import java.util.ArrayList;

import no.ntnu.imt3281.io.dataPackets.StartGame;
import no.ntnu.imt3281.ludo.logic.Ludo;

public class Game extends Ludo {
	private String name;
	private ArrayList<String> playerTokens = new ArrayList<>();
	private ArrayList<ClientConnection> players = new ArrayList<>();

	/**
	 * Create a new Game with the given name.
	 * 
	 * @param name the name of the game.
	 */
	public Game(String name) {
		this.name = name;
	}

	public ArrayList<ClientConnection> getPlayers() {
		return players;
	}
	
	public void addPlayer(String token, ClientConnection player) {
		playerTokens.add(token);
		players.add(player);
		addPlayer(player.getUsername());
	}

	public StartGame getStartGame() {
		StartGame startGame = new StartGame(name, this.hashCode());
		String playerNames[] = new String[players.size()];
		for (int i=0; i<players.size(); i++) {
			playerNames[i] = players.get(i).getUsername();
		}
		startGame.setPlayerNames(playerNames);
		return startGame;
	}
}
