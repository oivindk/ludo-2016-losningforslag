/**
 * This package includes all purely server related classes
 */
/**
 * @author Øivind Kolloen
 *
 */
package no.ntnu.imt3281.ludo.server;