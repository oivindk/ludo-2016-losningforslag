package no.ntnu.imt3281.ludo.server;

public class DuplicateUserException extends Exception {
	public DuplicateUserException(String msg, Exception throwable) {
		super(msg, throwable);
	}
}
