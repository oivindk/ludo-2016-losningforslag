package no.ntnu.imt3281.ludo.server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.io.dataPackets.Ping;

public class Pinger implements Runnable {
	HashMap<String, ClientConnection> connections = new HashMap<>();
	DatagramSocket socket;
	ActionListener removeUser;
	Logger logger = Logger.getLogger(getClass().getName());
	
	public Pinger (HashMap<String, ClientConnection> connections, DatagramSocket socket, ActionListener el) {
		this.connections = connections;
		this.socket = socket;
		this.removeUser = el;
	}
	
	private byte[] convertToBytes(Object object) throws IOException {
	    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
	         ObjectOutput out = new ObjectOutputStream(bos)) {
	        out.writeObject(object);
	        return bos.toByteArray();
	    } 
	}
	
	public void run() {
		while (true) {
			try {
				Ping p = new Ping();
				byte[] data = convertToBytes(p);
				connections.values().stream().forEach((c)->{
					DatagramPacket dp = new DatagramPacket(data, data.length, c.getSocketAddress());
					c.setPingReply(false);
					try {
						socket.send(dp);
					} catch (IOException e) {
						logger.log(Level.INFO, "Error sending PING", e);
					}
				});
				Thread.sleep(10000);	// Sending one ping every 30 seconds
				ArrayList<String> gone = new ArrayList<>(); 
				connections.entrySet().stream().forEach((entry)->{
					ClientConnection c = entry.getValue();
					if (!c.isPingReply()) {
						gone.add(entry.getKey());
					}
				});
				synchronized (connections) {
					for (String key : gone) {
						removeUser.actionPerformed (new ActionEvent(this, 1, key));
						connections.remove(key);
					}
				}
			} catch (Exception e) {
				logger.log(Level.INFO, "Error in Ping thread", e);;
			}
		}
	}
}
