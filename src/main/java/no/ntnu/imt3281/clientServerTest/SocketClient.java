package no.ntnu.imt3281.clientServerTest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketClient {
	ClientGUI gui = new ClientGUI();
	
	public SocketClient () {
		try (Socket s = new Socket("127.0.0.1", 6789)) {
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			gui.sendText.addActionListener(e->{
				try {
					bw.write(gui.sendText.getText());
					bw.newLine();
					bw.flush();
					System.out.println("Sent " + gui.sendText.getText());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			});
			ExecutorService pool = Executors.newFixedThreadPool(20);
			pool.execute(()-> {
				try {
					String tmp;
					while ((tmp = br.readLine())!=null) {
						gui.receivedText.setText(gui.receivedText.getText()+tmp+"\n");
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			});
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SocketClient client = new SocketClient();
		client.gui.setVisible(true);
	}
}
