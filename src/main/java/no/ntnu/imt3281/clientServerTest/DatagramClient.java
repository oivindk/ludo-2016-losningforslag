package no.ntnu.imt3281.clientServerTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import no.ntnu.imt3281.ludo.logic.DiceEvent;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.PieceEvent;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;

public class DatagramClient {
	ClientGUI gui = new ClientGUI();
	
	public DatagramClient () throws SocketException {
		@SuppressWarnings("resource")
		DatagramSocket socket = new DatagramSocket();
		gui.sendText.addActionListener(e->{
			try {
				String tmp = e.getActionCommand();
				byte[] data = convertToBytes(tmp);
				DatagramPacket dp = new DatagramPacket(data, data.length, new InetSocketAddress("127.0.0.1", 6789));
				socket.send(dp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		gui.button1.addActionListener(e->{
			DiceEvent ae = new DiceEvent(new Ludo(), 1, 1);
			try {
				byte[] data = convertToBytes(ae);
				DatagramPacket dp = new DatagramPacket(data, data.length, new InetSocketAddress("127.0.0.1", 6789));
				socket.send(dp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		gui.button2.addActionListener(e->{
			try {
				PlayerEvent pe = new PlayerEvent(new Ludo(), 1, 1);
				byte[] data = convertToBytes(pe);
				DatagramPacket dp = new DatagramPacket(data, data.length, new InetSocketAddress("127.0.0.1", 6789));
				socket.send(dp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		gui.button3.addActionListener(e->{
			try {
				PieceEvent pe = new PieceEvent(new Ludo(), 1, 1, 1, 5);
				byte[] data = convertToBytes(pe);
				DatagramPacket dp = new DatagramPacket(data, data.length, new InetSocketAddress("127.0.0.1", 6789));
				socket.send(dp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		byte[] buffer = new byte[1024*16];
		boolean stopping = false;
		while (!stopping) {
			DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
			try {
				socket.receive(dp);
				Object o = convertFromBytes(dp.getData());
				if (o instanceof String) {
					gui.receivedText.setText(gui.receivedText.getText()+o.toString()+"\n");
				} else if (o instanceof DiceEvent) {
					gui.receivedText.setText(gui.receivedText.getText()+"DiceEvent: "+o.toString()+"\n");
				} else if (o instanceof PlayerEvent) {
					gui.receivedText.setText(gui.receivedText.getText()+"PlayerEvent: "+o.toString()+"\n");
				} else if (o instanceof PieceEvent) {
					gui.receivedText.setText(gui.receivedText.getText()+"PieceEvent: "+o.toString()+"\n");
				} else if (o instanceof Ping) {
					gui.receivedText.setText(gui.receivedText.getText()+"Got Ping, sending Pong\n");
					Pong pong = new Pong();
					byte[] data = convertToBytes(pong);
					DatagramPacket dp1 = new DatagramPacket(data, data.length, new InetSocketAddress("127.0.0.1", 6789));
					socket.send(dp1);
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}			
		}
	}
	
	// from http://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array
	private byte[] convertToBytes(Object object) throws IOException {
	    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
	         ObjectOutput out = new ObjectOutputStream(bos)) {
	        out.writeObject(object);
	        return bos.toByteArray();
	    } 
	}
	
	// from http://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array
	private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
	    try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
	         ObjectInput in = new ObjectInputStream(bis)) {
	        return in.readObject();
	    } 
	}
	
	public static void main(String[] args) {
		try {
			DatagramClient client = new DatagramClient();
			client.gui.setVisible(true);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
