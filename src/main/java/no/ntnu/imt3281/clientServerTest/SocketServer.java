package no.ntnu.imt3281.clientServerTest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketServer {
	ServerGUI gui = new ServerGUI();
	ArrayList<Connection> clients = new ArrayList<>();
	
	public SocketServer () {
		ExecutorService pool = Executors.newCachedThreadPool();
		pool.execute(()-> {
		try {
			@SuppressWarnings("resource")
			ServerSocket ss = new ServerSocket(6789);
			Socket s;
			while ((s = ss.accept()) != null) {
				synchronized (clients) {
					clients.add(new Connection(s));
					System.out.println("Added client");					
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		});
		pool.execute(()-> {
			System.out.println("inside");
			while (true) {
				synchronized (clients) {
					for (Connection c : clients) {
						try {
							if (c.is.available()>0) {
								String tmp = c.br.readLine();
								send(tmp);
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}					
				}
			}
		});
		pool.shutdown();
	}

	private void send(String tmp) {
		gui.receivedText.setText(gui.receivedText.getText()+"Send : ("+tmp+") to "+clients.size()+" clients\n");
		for (Connection c : clients) {
			try {
				c.bw.write(tmp);
				c.bw.newLine();
				c.bw.flush();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		SocketServer server = new SocketServer();
		server.gui.setVisible(true);
	}
	
	class Connection {
		Socket s;
		InputStream is;
		BufferedReader br;
		BufferedWriter bw;
		
		public Connection (Socket s) {
			this.s = s;
			try {
				is = s.getInputStream();
				br = new BufferedReader(new InputStreamReader(is));
				bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
