package no.ntnu.imt3281.clientServerTest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.HashMap;

public class Pinger extends Thread {
	HashMap<String, Connection> connections = new HashMap<>();
	DatagramSocket socket;
	
	public Pinger (HashMap<String, Connection> connections, DatagramSocket socket) {
		this.connections = connections;
		this.socket = socket;
	}
	
	private byte[] convertToBytes(Object object) throws IOException {
	    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
	         ObjectOutput out = new ObjectOutputStream(bos)) {
	        out.writeObject(object);
	        return bos.toByteArray();
	    } 
	}
	
	public void run() {
		while (true) {
			try {
				Thread.sleep(50);
				System.out.println("Hei");
				Ping p = new Ping();
				byte[] data = convertToBytes(p);
				connections.values().stream().forEach(c->{
					System.out.println("Sender Ping");
					DatagramPacket dp = new DatagramPacket(data, data.length, c.address);
					c.pingReply = false;
					try {
						socket.send(dp);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
				Thread.sleep(4000);
				ArrayList<String> gone = new ArrayList<>(); 
				connections.entrySet().stream().forEach((entry)->{
					Connection c = entry.getValue();
					if (!c.pingReply) {
						gone.add(entry.getKey());
					}
				});
				/*synchronized (connections) {
					for (String key : gone) {
						System.out.println("Pong timeout: " + key);
						connections.remove(key);
					}
				}*/
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
