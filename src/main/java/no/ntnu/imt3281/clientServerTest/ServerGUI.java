package no.ntnu.imt3281.clientServerTest;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ServerGUI extends JFrame {
	JTextArea receivedText = new JTextArea();
	
	public ServerGUI () {
		super ("Server");
		add (new JScrollPane(receivedText));
		setSize(400, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}
