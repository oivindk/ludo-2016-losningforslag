package no.ntnu.imt3281.clientServerTest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;

public class DatagramServer {
	HashMap<String, Connection> connections = new HashMap<>();
	private DatagramPacket packet;
	ServerGUI gui = new ServerGUI();
	boolean stopping = false;
	
	public DatagramServer () {
		try {
			DatagramSocket socket = new DatagramSocket(6789);
			byte[] buffer = new byte[1024*16];
			packet = new DatagramPacket(buffer, buffer.length);
			Pinger pinger = new Pinger(connections, socket);
			pinger.setDaemon(true);
			pinger.start();
			while (!stopping) {
				try {
					socket.receive(packet);
					Object o = convertFromBytes(packet.getData());
					if (o instanceof Pong) {
						connections.get(packet.getAddress().toString()+packet.getPort()).pingReply = true;
					} else {
						if (!connections.containsKey(packet.getAddress().toString()+packet.getPort())) {
							connections.put(packet.getAddress().toString()+packet.getPort(), new Connection(packet));
							gui.receivedText.setText(gui.receivedText.getText()+"New connection from : "+packet.getAddress()+":"+packet.getPort()+"\n");
						}
						gui.receivedText.setText(gui.receivedText.getText()+"Content of package : "+packet.getLength()+" bytes\n");
						synchronized (connections) {
							connections.values().stream().forEach((c)->{
								gui.receivedText.setText(gui.receivedText.getText()+"Sending to :"+c.address+"\n");
								DatagramPacket dp = new DatagramPacket(packet.getData(), packet.getLength(), c.address);
								try {
									socket.send(dp);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							});							
						}
					}
				} catch (IOException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	// from http://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array
	private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
	    try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
	         ObjectInput in = new ObjectInputStream(bis)) {
	        return in.readObject();
	    } 
	}
	
	public static void main(String[] args) {
		DatagramServer server = new DatagramServer();
		server.gui.setVisible(true);
	}
}
