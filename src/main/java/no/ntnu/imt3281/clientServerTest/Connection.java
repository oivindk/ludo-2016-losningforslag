package no.ntnu.imt3281.clientServerTest;

import java.net.DatagramPacket;
import java.net.SocketAddress;

class Connection {
	SocketAddress address;
	boolean pingReply = true;

	public Connection(DatagramPacket packet) {
		address = packet.getSocketAddress();
	}
}