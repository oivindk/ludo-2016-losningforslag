package no.ntnu.imt3281.clientServerTest;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ClientGUI extends JFrame {
	JTextField sendText = new JTextField(30);
	JTextArea receivedText = new JTextArea();
	JButton button1;
	JButton button2;
	JButton button3;
	
	
	public ClientGUI () {
		super ("Client");
		add (new JScrollPane(receivedText));
		JPanel p = new JPanel(new GridLayout(2, 1));
		JPanel buttons = new JPanel(new GridLayout(1, 3));
		button1 = new JButton("Send event 1");
		buttons.add(button1);
		button2 = new JButton("Send event 2");
		buttons.add(button2);
		button3 = new JButton("Send event 3");
		buttons.add(button3);
		p.add(buttons);
		p.add(sendText);
		add (p, BorderLayout.SOUTH);
		setSize(400, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}
