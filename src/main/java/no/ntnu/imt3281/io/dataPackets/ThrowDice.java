package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Used to request a throwDice when sent from client, provides a diceValue when sent 
 * from server.
 * 
 * @author okolloen
 *
 */
public class ThrowDice implements Serializable {
	private static final long serialVersionUID = 1L;
	private int gameId;
	private int value=-1;

	/**
	 * When created on the client only the game id is provided.
	 * 
	 * @param gameId the id of the game that a dice should be thrown in.
	 */
	public ThrowDice(int gameId) {
		this.gameId = gameId;
	}

	/**
	 * When created on the server both the game id and the value of the dice thrown is provided.
	 * 
	 * @param gameId the id of the game that the dice was thrown in.
	 * @param value the value of the dice.
	 */
	public ThrowDice(int gameId, int value) {
		super();
		this.gameId = gameId;
		this.value = value;
	}

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
}