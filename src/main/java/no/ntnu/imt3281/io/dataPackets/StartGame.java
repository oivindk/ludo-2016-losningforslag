package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * This object is sent to clients to start a new game.
 * 
 * @author okolloen
 */
public class StartGame implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private int gameCode;
	private String[] playerNames;

	/**
	 * Create an object with the name of the game and the game id
	 * 
	 * @param name the name given to this game. This is either "Random game" or 
	 * "Challenge: name of player initiating the game challenge" 
	 * @param gameCode id for this game
	 */
	public StartGame(String name, int gameCode) {
		this.name = name;
		this.gameCode = gameCode;
	}

	/**
	 * Set the names of the players for this game.
	 * 
	 * @param playerNames an array containing the names of the players in this game.
	 */
	public void setPlayerNames(String[] playerNames) {
		this.playerNames = playerNames;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the playerNames
	 */
	public String[] getPlayerNames() {
		return playerNames;
	}

	/**
	 * Get the unique id for this game.
	 * 
	 * @return the gameCode
	 */
	public int getGameCode() {
		return gameCode;
	}
}
