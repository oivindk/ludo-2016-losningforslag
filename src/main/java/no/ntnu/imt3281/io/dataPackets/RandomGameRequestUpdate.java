package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from the server once every second to players waiting to join a game.
 * These messages are sent only when there are two or more players waiting to join a game.
 * @author oivindk
 *
 */
public class RandomGameRequestUpdate implements Serializable {
	private static final long serialVersionUID = 1L;
	private int timeRemaining;
	private int playersRegistered;

	/**
	 * Create object with information about time remaining and players registered.
	 * 
	 * @param timeRemaining counts down from 30 seconds, the time limit to wait for new players.
	 * @param playersRegistered the number of players waiting for a random game to be started.
	 */
	public RandomGameRequestUpdate(int timeRemaining, int playersRegistered) {
		this.timeRemaining = timeRemaining;
		this.playersRegistered = playersRegistered;
	}

	/**
	 * @return the timeRemaining
	 */
	public int getTimeRemaining() {
		return timeRemaining;
	}

	/**
	 * @return the playersRegistered
	 */
	public int getPlayersRegistered() {
		return playersRegistered;
	}

}
