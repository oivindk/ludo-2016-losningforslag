package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from a client if the user tires of waiting for a random game to start.
 * This means the user will leave the random game que on the server.
 * 
 * @author oivindk
 *
 */
public class CancelRandomGame implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;

	/**
	 * Only need the user token for the user leaving the game to 
	 * perform this operation.
	 * 
	 * @param token the user token.
	 */
	public CancelRandomGame(String token) {
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
}
