package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from the server to clients when a user enter or leave a chat room that
 * this client is a part of.
 * 
 * @author oivindk
 *
 */
public class ChatParticipant implements Serializable {
	private static final long serialVersionUID = 1L;
	/** Indicates that this object represents a user joining a channel */
	public static final int JOIN = 0;
	/** Indicates that this object represents a user leaving a channel */
	public static final int LEAVE = 1;
	private String userName;
	private int action;
	private String channelName;

	/**
	 * Creates a new object with all required data.
	 * 
	 * @param channelName the channel where the user is entering/leaving
	 * @param userName the name of the user
	 * @param action JOIN or LEAVE.
	 */
	public ChatParticipant(String channelName, String userName, int action) {
		this.channelName = channelName;
		this.userName = userName;
		this.action = action;
	}

	
	/**
	 * @return the channelName
	 */
	public String getChannelName() {
		return channelName;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the action
	 */
	public int getAction() {
		return action;
	}
}
