package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from a client to the server letting the server know that
 * this client is no longer on this channel
 * 
 * @author oivindk
 *
 */
public class LeaveChannel implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;
	private String channelName;

	/**
	 * Create object with all required information.
	 * 
	 * @param token the token identifying this user to the server.
	 * @param channelName the name of the channel
	 */
	public LeaveChannel(String token, String channelName) {
		this.token = token;
		this.channelName = channelName;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the channelName
	 */
	public String getChannelName() {
		return channelName;
	}
}
