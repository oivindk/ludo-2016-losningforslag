package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;
import java.util.ArrayList;

import no.ntnu.imt3281.ludo.server.ClientConnection;

/**
 * Sent from the server to a client when the user on that client has
 * asked to join a channel.
 * 
 * Contains the name of the channel and a list of users (usernames) of
 * users on the channel.
 * 
 * @author oivindk
 *
 */
public class ChannelInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	String channelName;
	ArrayList<String> users = new ArrayList<>();

	/**
	 * Takes the name of the channel and a list of @see ClientConnection objects as parameters.
	 * The usernames is gathered from the ClientConnection objects and added to a list of Strings.
	 * 
	 * @param channelName the name of the new channel
	 * @param clients the users of this channel
	 */
	public ChannelInfo(String channelName, ArrayList<ClientConnection> clients) {
		this.channelName = channelName;
		for (ClientConnection client : clients) 
			users.add(client.getUsername());
	}

	/**
	 * @return the channelName
	 */
	public String getChannelName() {
		return channelName;
	}

	/**
	 * @return the users
	 */
	public ArrayList<String> getUsers() {
		return users;
	}
}
