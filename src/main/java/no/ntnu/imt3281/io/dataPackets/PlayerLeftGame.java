package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent clients and server when a user has left a game.
 * Can be triggered by missing ping reply or a client sends it when 
 * a user closes a game tab.
 * 
 * @author oivindk
 *
 */
public class PlayerLeftGame implements Serializable {
	private static final long serialVersionUID = 1L;
	private int gameId;
	private String username;

	/**
	 * When created on the server the gameId and username of the player
	 * leaving the game is set, when created on a client the client token
	 * us used instead of the username.
	 * 
	 * @param gameId the id of the game where the user left
	 * @param username the username of the user leaving the game OR
	 * the token of the user leaving.
	 */
	public PlayerLeftGame(Integer gameId, String username) {
		this.gameId = gameId;
		this.username = username;
	}

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * This method is used when the object is received on the client
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * This method is used when the object is received on the server.
	 * 
	 * @return the token for this user.
	 */
	public Object getToken() {
		return username;
	}
}
