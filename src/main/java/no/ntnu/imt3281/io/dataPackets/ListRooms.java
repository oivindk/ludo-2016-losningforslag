package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Sent from client to server to request list of rooms, sent
 * from server to client with a list of rooms.
 * 
 * @author oivindk
 *
 */
public class ListRooms implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;
	HashMap<String, Integer> roomParticipants;

	/**
	 * Used on client to create a new object requesting to 
	 * the server to send a list of all chat rooms available.
	 * 
	 * @param token identify this user to the server.
	 */
	public ListRooms(String token) {
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Used on the server to add a list of rooms to the object before
	 * sending it back to the client.
	 * 
	 * @param roomParticipants a map mapping channel names to the number of
	 * participants in that room.
	 */
	public void setRooms(HashMap<String, Integer> roomParticipants) {
		this.roomParticipants = roomParticipants;
	}

	/**
	 * @return the roomParticipants, a map mapping the room names to the number
	 * of participants in that room.
	 */
	public HashMap<String, Integer> getRoomParticipants() {
		return roomParticipants;
	}
}
