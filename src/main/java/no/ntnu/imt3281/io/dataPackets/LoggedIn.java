package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from server to client to notify the client that it is logged in.
 * Contains newly created token that the client is to send with all future requests.
 * 
 * @author Øivind Kolloen
 *
 */
public class LoggedIn implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;

	/**
	 * Send this message to clients with the token that the client is to 
	 * us for all future communication with the server.
	 * 
	 * @param token is to be used for all future communication with the server.
	 * This is this clients identification that let the server keep track of the 
	 * different connected clients.
	 */
	public LoggedIn(String token) {
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
}
