package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Used to send chat messages between server and client.
 * For master chat channel messages the channel is null, otherwise it contains the channel id.
 * For messages sent from the clients to the server the token is set, and for messages sent 
 * the other way the username is set.
 * 
 * @author okolloen
 *
 */
public class ChatMessage implements Serializable{
	private static final long serialVersionUID = 1L;
	private String message;
	private String token;
	private String channel = null;
	private String username;

	/**
	 * Used on client to create a master channel message.
	 * 
	 * @param message the message to send.
	 * @param token the token for this client.
	 */
	public ChatMessage(String message, String token) {
		this.message = message;
		this.token = token;
	}

	/**
	 * Used on server to create a master channel message.
	 * 
	 * @param message the message to send
	 * @param token the users token
	 * @param username the username of the user sending the message
	 */
	public ChatMessage(String message, String token, String username) {
		this.message = message;
		this.username = username;
		this.token = token;
	}

	/**
	 * Used to create a message to a specific channel. Used both on client and on server.
	 * @param message the message to send
	 * @param token the users token
	 * @param username the username that sent the message. (null if from client.)
	 * @param channel the name of the channel this message is for
	 */
	public ChatMessage(String message, String token, String username, String channel) {
		this.channel = channel;
		this.message = message;
		this.username = username;
		this.token = token;
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
}
