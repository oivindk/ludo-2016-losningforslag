package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from client to server requesting that this user be added to a random game.
 * The user will receive updates when there are two or more (including the user sending
 * this message) waiting to join a game.
 * 
 * @author oivindk
 *
 */
public class JoinRandomGame implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;

	/**
	 * Identify the client with the user token.
	 * 
	 * @param token the token identifying the user to the server.
	 */
	public JoinRandomGame(String token) {
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
}
