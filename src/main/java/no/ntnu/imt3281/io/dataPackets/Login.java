package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from Client to server to request login.
 * Contains the username and password for the user.
 * 
 * @author Øivind Kolloen
 *
 */
public class Login implements Serializable {
	private static final long serialVersionUID = 1L;
	String username;
	String password;

	/**
	 * Sent to server to request login
	 * 
	 * @param username for user logging in
	 * @param password for user logging in
	 */
	public Login(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
}
