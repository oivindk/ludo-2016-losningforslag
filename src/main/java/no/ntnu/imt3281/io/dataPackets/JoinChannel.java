package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from the client to the server, requesting to join this channel.
 * 
 * @author okolloen
 *
 */
public class JoinChannel implements Serializable{
	private static final long serialVersionUID = 1L;
	private String token;
	private String channelName;

	/**
	 * Create object with all required data.
	 * 
	 * @param token the token identifying the user.
	 * @param channelName the name of the channel to join.
	 */
	public JoinChannel(String token, String channelName) {
		this.token = token;
		this.channelName = channelName;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the channelName
	 */
	public String getChannelName() {
		return channelName;
	}
}
