/**
 * Defines all datapackets (classes) sent between client and server.
 */
/**
 * @author okolloen
 *
 */
package no.ntnu.imt3281.io.dataPackets;