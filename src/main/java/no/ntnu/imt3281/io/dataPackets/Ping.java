package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent to clients, expecting Pong as response
 * 
 * @author Øivind Kolloen
 *
 */
public class Ping implements Serializable {
	private static final long serialVersionUID = 1L;
}
