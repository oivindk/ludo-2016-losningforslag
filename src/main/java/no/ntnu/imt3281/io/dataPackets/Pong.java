package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent to server in response to a Ping
 * 
 * @author Øivind Kolloen
 *
 */
public class Pong implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;
	
	/**
	 * Need to include the user/client token to let the server know 
	 * which client responds to the ping.
	 * 
	 * @param token identifies this client to the server.
	 */
	public Pong(String token) {
		this.token = token;
	}
	
	/**
	 * 
	 * @return the user token
	 */
	public String getToken() {
		return token;
	}
}
