package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Used to send message from a client to the server to move a piece 
 * in a given game.
 * 
 * @author okolloen
 *
 */
public class MovePiece implements Serializable {
	private static final long serialVersionUID = 1L;
	private int gameId;
	private int player;
	private int from;
	private int to;

	/**
	 * Create a new MovePiece object with given values.
	 * 
	 * @param gameId the id of the game.
	 * @param player the player making the omve.
	 * @param from where the piece is moved from.
	 * @param to where the piece is moved to.
	 */
	public MovePiece(int gameId, int player, int from, int to) {
		this.gameId = gameId;
		this.player = player;
		this.from = from;
		this.to = to;
	}

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @return the player
	 */
	public int getPlayer() {
		return player;
	}

	/**
	 * @return the from
	 */
	public int getFrom() {
		return from;
	}

	/**
	 * @return the to
	 */
	public int getTo() {
		return to;
	}
}