package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from client to server to generate a new user.
 */
public class CreateUser implements Serializable {
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String givenname;
	private String surename;
	private String email;

	/**
	 * Create object with all required information
	 * 
	 * @param username the username for the user to create
	 * @param password the password for this new user
	 * @param givenname the first name of the user
	 * @param surename the family name of the user
	 * @param email the email address of the user
	 */
	public CreateUser(String username, String password, String givenname, String surename, String email) {
		this.username = username;
		this.password = password;
		this.givenname = givenname;
		this.surename = surename;
		this.email = email;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the givenname
	 */
	public String getGivenname() {
		return givenname;
	}

	/**
	 * @return the surename
	 */
	public String getSurename() {
		return surename;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
}
