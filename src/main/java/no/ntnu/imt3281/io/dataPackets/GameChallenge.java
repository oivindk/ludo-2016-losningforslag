package no.ntnu.imt3281.io.dataPackets;

import java.io.Serializable;

/**
 * Sent from a client to challenge players to a game.
 * 
 * @author oivindk
 *
 */
public class GameChallenge implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;
	private String[] players;

	/**
	 * 
	 * @param token the token for the user performing the challenge
	 * @param players the names of the players being challenged. 
	 * This might or might not include the name of the player performing the challenge.
	 */
	public GameChallenge(String token, String[] players) {
		this.token = token;
		this.players = players;
	}

	/**
	 * @return the token of the player performing the challenge
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the players as an array of strings with user name of players
	 */
	public String[] getPlayers() {
		return players;
	}
	
}
