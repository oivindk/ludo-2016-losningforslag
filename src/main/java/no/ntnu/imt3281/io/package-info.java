/**
 * IO related classes will be placed in this package.
 * 
 * The Tools class contains methods to convert between objects and byte arrays.
 */
/**
 * @author okolloen
 *
 */
package no.ntnu.imt3281.io;