package no.ntnu.imt3281.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Utility class containing methods for converting between objects and byte arrays.
 * 
 * @author oivindk
 *
 */
public class Tools {
	/**
	 * Prevent creating objects of this class
	 */
	private Tools() {
		
	}
	
	/**
	 * Serialize object and return array of bytes.
	 * 	
	 * // from http://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array 
	 * @param object the object to be serialized
	 * @return the byte array containing the serialized form of the object
	 * 
	 * @throws IOException if object can not be serialized
	 */
	public static byte[] convertToBytes(Object object) throws IOException {
	    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
	         ObjectOutput out = new ObjectOutputStream(bos)) {
	        out.writeObject(object);
	        return bos.toByteArray();
	    } 
	}
	
	/**
	 * Convert from an array of bytes to an object (de-serialize an object).
	 * 
	 * // from http://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array
	 * @param bytes the array of bytes containing the objet.
	 * @return the object from the array of bytes
	 * @throws ObjectConversionException in case of error converting from bytes to object
	 */
	public static Object convertFromBytes(byte[] bytes) throws ObjectConversionException {
	    try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
	         ObjectInput in = new ObjectInputStream(bis)) {
	        return in.readObject();
	    } catch (ClassNotFoundException e) {
			throw new ObjectConversionException (e);
		} catch (IOException ioe) {
			throw new ObjectConversionException (ioe);
		}
	}
}
