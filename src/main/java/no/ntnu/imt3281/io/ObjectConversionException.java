package no.ntnu.imt3281.io;

/**
 * Thrown when trying to convert from bytes to an object and this fails.
 * 
 * @author oivindk
 *
 */
public class ObjectConversionException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * @see java.lang.Exception
	 * 
	 * @param msg the message for this exception 
	 */
	public ObjectConversionException (String msg) {
		super(msg);
	}

	/**
	 * @see java.lang.Exception
	 * 
	 * @param e base this exception on another exception
	 */
	public ObjectConversionException(Exception e) {
		super(e);
	}
}