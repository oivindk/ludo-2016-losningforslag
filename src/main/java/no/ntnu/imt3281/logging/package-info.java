/**
 * Package contains enhanced logging functionality. Used on client only.
 * 
 * From https://bitbucket.org/okolloen/imt3281/ 08-Logging
 */
/**
 * @author oivindk
 *
 */
package no.ntnu.imt3281.logging;