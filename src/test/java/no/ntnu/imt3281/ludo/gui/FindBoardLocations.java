package no.ntnu.imt3281.ludo.gui;

import java.util.ResourceBundle;
import java.util.logging.Level;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.io.dataPackets.StartGame;
import no.ntnu.imt3281.logging.Logger;
import no.ntnu.imt3281.ludo.gui.GameBoard;

/**
 * Used while finding the top/left corners of all the squares where user pieces can be placed.
 * 
 * @author oivindk
 *
 */
public class FindBoardLocations extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
	    	FXMLLoader loader = new FXMLLoader(getClass().getResource("../gui/GameBoard.fxml"));
	    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

			ScrollPane root = (ScrollPane)loader.load();
			GameBoard ludo = loader.getController();
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.setTitle(I18N.getString("mainWindow.title"));
			primaryStage.show();
			StartGame game = new StartGame("test", 1);
			String players[] = {"Player 1", "Player 2"};
			game.setPlayerNames(players);
			ludo.initializeGame(game);
		} catch(Exception e) {
			Logger.getLogger().log(Level.SEVERE, "Unable to load gameboard definition", e);
			System.exit(-2);
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
