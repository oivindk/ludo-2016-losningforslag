package no.ntnu.imt3281.ludo.server;

import static org.junit.Assert.*;

import org.junit.Test;

public class DatabaseTest {
	static Database database = Database.getDatabase();
	
	@Test
	public void loginTest() {
		assertNotNull(database.login("kalle", "hemmelig"));
		assertNull(database.login("kalle", "hemmmelig"));
	}
	
	@Test
	public void validateTokenTest() {
		String token = database.login("kalle", "hemmelig");
		assertEquals(-1, database.validateToken(token+"."),0 );
	}
	
	@Test
	public void getUserInfoTest() {
		String token = database.login("kalle", "hemmelig");
		String info[] = database.getUserInfo(database.validateToken(token));
		assertEquals("kalle", info[0]);
		//assertEquals("", info[1]);
		//assertEquals("", info[2]);
		//assertEquals("", info[3]);
	}
}
