# Velkommen til prosjekt 2 (Ludo prosjektet) i IMT3281 Applikasjonsutvikling

# Løsningsforslag utarbeidet av Øivind Kolloen

Opprett en fork av dette repositoriet. Inviter resten av gruppen inn som deltakere på det repositoriet du har forket. Last ned en lokal kopi av repositoriet (git clone) og importer prosjektet i din IDE som et maven prosjekt. Oppdater .gitignore med de filene som ble opprettet når du importerte prosjektet til din IDE.

Du er nå klar til å begynne på prosjektet :-).

[Beskrivelse av prosjektet](https://bitbucket.org/okolloen/imt3281-project2-2016/wiki/Home)

All testkode skal passere for å få bestått prosjektet, koden som ligger i src/main/java er gitt som et utgangspunkt, dere kan enten bygge videre på den eller starte fra scratch.
